package com.example.studyprojkot.ex_koin.first_way

import android.util.Log
import com.example.studyprojkot.ex_koin.first_way.SchoolService

class StudentController(val schoolService: SchoolService) {

    fun print() {
        Log.d("yck", "현재재학중인 학교  : ${schoolService.mySchoolName}")
    }

}