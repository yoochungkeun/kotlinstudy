package com.example.studyprojkot.ex_koin.second_way

import android.os.Bundle
import com.example.studyprojkot.BaseActivity
import org.koin.android.ext.android.inject

class KoinActivitySecond: BaseActivity() {

    val testAction:TestAction by inject()
    val testDoubleAction:TestDoubleAction by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        testAction.Punch()

        testDoubleAction.DoublePunch()

        testDoubleAction.DoubleKick()
/*
        var testAction = TestAction()
        testAction.Punch()
*/


    }


}//class end