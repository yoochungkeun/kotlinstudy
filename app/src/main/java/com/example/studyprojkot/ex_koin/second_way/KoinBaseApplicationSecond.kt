package com.example.studyprojkot.ex_koin.second_way

import android.app.Application
import org.koin.android.ext.android.startKoin


class KoinBaseApplicationSecond:Application(){

    override fun onCreate() {
        super.onCreate()
        startKoin(applicationContext, myDiModule)
    }

}//class end