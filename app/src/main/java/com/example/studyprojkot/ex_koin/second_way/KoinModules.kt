package com.example.studyprojkot.ex_koin.second_way

import org.koin.dsl.module.module

//DSL 키워드
/*
- module : Koin모듈을 정의할때 사용
- factory : inject하는 시점에 해당 객체를 생성
- single : 앱이 살아있는 동안 전역적으로 사용가능한 객체를 생성
- bind : 생성할 객체를 다른 타입으로 바인딩 하고 싶을때 사용
- get : 주입할 각 컴포넌트끼리의 의존성을 해결하기 위해 사용
*/

var actionModule = module {
    factory {
        TestAction()
    }
}

var doubleActionModule = module {
    factory {
        TestDoubleAction()
    }
}



var myDiModule = listOf(actionModule, doubleActionModule)


