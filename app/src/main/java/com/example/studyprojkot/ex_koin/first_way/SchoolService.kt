package com.example.studyprojkot.ex_koin.first_way

class SchoolService {

    var mySchoolName = "배재대학교"

    fun moveSchool(newSchoolName: String) {
        this.mySchoolName = newSchoolName
    }

}//class end