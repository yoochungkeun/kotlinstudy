package com.example.studyprojkot.ex_koin.first_way

import android.os.Bundle
import com.example.studyprojkot.BaseActivity
import com.example.studyprojkot.R
import org.koin.android.ext.android.inject

class KoinActivityMain : BaseActivity() {

    private val schoolService: SchoolService by inject()
    private val studentController: StudentController by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ex_koin_main)




        //koin 의존성 주입후 출력
        schoolService.moveSchool("중국대련동북재경대학교")

        studentController.print()


/*
        //일반 클래스 객체생성후 출력
        var schoolService = SchoolService()
        schoolService.moveSchool("중국대련이공대학교")
        var studentController = StudentController(schoolService)
        studentController.print()
*/


    }

}//class end