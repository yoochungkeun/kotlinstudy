package com.example.studyprojkot.ex_data_structure

import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap

class DataStructureActivityMain : AppCompatActivity(){

    //*자료구조 대표적인 클래스 collection , Map 으로
    //Collection{List{ArrayList,LinkedList},{Set{HashSet,TreeSet,LinkedHashSet,SortedSet}}}
    //Map{HashMap,TreeMap,LinkedHashMap}

    // Graph ?

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //List
        immutable_List()
        mutable_List()
        mutable_arrayList()//data 찾는데 O(1) 복잡도를 가짐
        mutable_linkedList()

        //Set
        immutable_Set()
        mutable_Set()
        mutable_HashSet()
        mutable_TreeSet()
        mutable_LinkedHashSet()
        mutable_SortedSet()

        //Map
        immutable_Map()
        mutable_Map()
        mutable_HashMap()
        mutable_TreeMap()
        mutable_LinkedHashMap()
    }





    //======== ▼▼ ========= ======== ▼▼ ========= List 계열 ======== ▼▼ ========= ======== ▼▼ =========
    //불변 List
    fun immutable_List(){
        //- List 는 기본 Any 형태로 지정된다.(다양한 자료형을 하나의 list 에 담을수 있다, 추출시 Any 형태로 가져온다.
        //- List 는 한번 정의되면 add 나 remove 와 같이 변경이 불가능함
        //- List 는 최초 정의 후 , 추출만 가능하다
        //- List 는 중복 허용
        var exList = listOf("stringValue",0,1.3,true,1.3)
        for(i in 0 until exList.size){
            Log.d("immutable_List","mylist[${i}] : ${exList[i]}")
        }
        //- List 의 사용
        val get_list_value = exList[0]//0번째값 추출
        val get_list_value2 = exList.indexOf(0)//0번째값 추출
        val contain_list_value = exList.contains("stringValue") //"stringValue" 있는지 확인
    }

    //가변 List
    fun mutable_List(){
        //- List(mutable) 는 기본 Any 형태로 지정된다.(다양한 자료형을 하나의 list 에 담을수 있다, 추출시 Any 형태로 가져온다.
        //- List(mutable) 는 정의후 add 나 remove 사용 가능하다.
        //- List(mutable) 는 중복 허용
        var exListMutable = mutableListOf("stringValue",0,1.3,true,0)
        for(i in 0 until exListMutable.size){
            Log.d("mutable_List","exListMutable[${i}] : ${exListMutable[i]}")
        }
        //- List(mutable)의 사용
        exListMutable.add("stringValue")//stringValueChange 값 추가
        exListMutable.add(0)//0 값 추가
        exListMutable.remove(0)//0번째값 제거
        val tempListMutable = mutableListOf("tempStringValue",2,4.5,false)
        exListMutable.addAll(tempListMutable)//tempListsMutable 를 통째로 추가
        val get_mutable_list_value = exListMutable[0]//0번째값 추출
        val get_mutable_list_value2 = exListMutable.indexOf(0)//0번째값 추출
        val contain_mutable_list_value = exListMutable.contains("stringValue") //"stringValue" 있는지 확인
        for(i in 0 until exListMutable.size){
            Log.d("mutable_List222","exListMutable[${i}] : ${exListMutable[i]}")
        }
    }

    //가변 ArrayList
    fun mutable_arrayList(){
        //- ArrayList 는 기본 Any 형태로 지정된다.(다양한 자료형을 하나의 list 에 담을수 있다, 추출시 Any 형태로 가져온다.
        //- ArrayList 는 정의후 add 나 remove 사용 가능하다.
        val exArrlist = ArrayList<String>()
        exArrlist.add("리스트1")
        exArrlist.add("리스트2")
        exArrlist.add("리스트3")
        exArrlist.add("리스트4")
        exArrlist.add("리스트5")

        val exArrlist2 = ArrayList<String>()
        exArrlist2.add("리스트1")
        exArrlist2.add("리스트4")
        exArrlist2.add("리스트5")
        exArrlist2.add("리스트6")

        for(i in 0 until exArrlist.size){
            Log.d("exArrlist","exArrlist[${i}] : ${exArrlist[i]}")
        }
        //- ArrayList 의 사용
        exArrlist.retainAll(exArrlist2)//exArrlist 에서 exArrlist2 와 공통 된 부분만 남기고 나머지 제거
        val get_arrlist_value = exArrlist[0]//0번째값 추출
        Log.d("exArrlist","get_arrlist_value : ${get_arrlist_value}")
        val get_arrlist_value2 = exArrlist.indexOf("리스트5")//"리스트5"의 index 값 추출
        Log.d("exArrlist","get_arrlist_value2 : ${get_arrlist_value2}")
        val get_arrlist_value3 = exArrlist.contains("stringValue") //"stringValue" 있는지 확인
        Log.d("exArrlist","get_arrlist_value3 : ${get_arrlist_value3}")
    }

    //가변 LinkedList
    fun mutable_linkedList(){
        //- LinkedList(mutable) 는 기본 Any 형태로 지정된다.(다양한 자료형을 하나의 list 에 담을수 있다, 해당 자료형 그대로 저장한다.(Int ,String 섞어서 저장 가능)
        //- LinkedList(mutable) 는 정의후 add 나 remove 사용 가능하다.
        //- LinkedList(mutable) 는 중복 허용
        var exLinkelistMutable = LinkedList<Any>()

        exLinkelistMutable.add("링크드리스트1")
        exLinkelistMutable.add("링크드리스트2")
        exLinkelistMutable.add(0)
        exLinkelistMutable.add("링크드리스트3")
        exLinkelistMutable.add(false)
        exLinkelistMutable.add("링크드리스트4")
        exLinkelistMutable.add("링크드리스트4")

        for(i in 0 until exLinkelistMutable.size){
            Log.d("mutable_linkedList","exLinkelistMutable[${i}] : ${exLinkelistMutable[i]}")
            Log.d("mutable_linkedList","is Int [${i}] : ${exLinkelistMutable[i] is Int}")
            Log.d("mutable_linkedList","is String [${i}] : ${exLinkelistMutable[i] is String}")
            Log.d("mutable_linkedList","is Boolean [${i}] : ${exLinkelistMutable[i] is Boolean}")
        }

    }

    //======== ▼▼ ========= ======== ▼▼ ========= Map 계열 ======== ▼▼ ========= ======== ▼▼ =========
    //불변 Map
    fun immutable_Map(){
        //- Map 은 Key,Value 형태로 사용된다. (다양한 자료형을 하나의 Map 에 담을수 있다, 추출시 Key,Value 형태로 가져온다.
        //- Map 는 한번 정의되면 put(add) 이나 remove 와 같이 변경이 불가능함
        //- Map 은 Key는 중복이 불가하지만, Value 는 중복이 가능하다.
        var exMap = mapOf("BBQ자메이카통다리" to 24000,"함흥식비빔냉면" to 12000,50000 to "소고기",true to "주문완료",'A' to 'Z')
        //- Map 의 사용
        val get_map_value = exMap.get("BBQ자메이카통다리")//"BBQ자메이카통다리"키의 값 추출
        val get_map_value2 = exMap.get("함흥식비빔냉면")//"함흥식비빔냉면"키의 값 추출
        val get_map_value3 = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            exMap.getOrDefault("함흥식비빔냉면","No")
        } else {
            //"함흥식비빔냉면"번째값 추출
        }
    }

    //가변 Map
    fun mutable_Map(){
        //- Map(mutbable) 은 Key,Value 형태로 사용된다. (다양한 자료형을 하나의 Map 에 담을수 있다, 추출시 Key,Value 형태로 가져온다.
        //- Map(mutable) 은 정의후 put 이나 remove 사용 가능하다.
        //- Map(mutable) 은 Key는 중복이 불가하지만, Value 는 중복이 가능하다.
        //- Map(mutable) 은 순서가 보장 된다.(add 순서대로 출력된다.(내림차순))
        //- iterator 로도 값을 가져 올 수있다.
        var exMapMutbale = mutableMapOf("BBQ자메이카통다리음식" to 24000,"함흥식비빔냉면음식" to 12000,"소고기갈비살음식" to 50000 ,true to "주문완료",'A' to 'Z')

        var itr = exMapMutbale.iterator()//iterator 사용하여 내부 저장값을 볼 수 있다.(모든 Collection 안에는 iterator 가 존재함)
        while(itr.hasNext()){
            var itrt = itr.next()
            var getKey = itrt.key
            Log.d("mutable_Map","getKey : ${getKey}")
        }
        //- Map 의 사용
        exMapMutbale.put("처갓집양념통닭", 24000)//값 새로 추가
        exMapMutbale[50000] = "소고기부채살" //50000 값 -> "소고기부채살" 로 변경
        exMapMutbale.remove('A')//A값 제거
        val get_map_value = exMapMutbale.get("BBQ자메이카통다리")//"BBQ자메이카통다리"키의 값 추출
        val get_map_value2 = exMapMutbale.get("함흥식비빔냉면")//"함흥식비빔냉면"키의 값 추출
        val get_map_value3 = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            exMapMutbale.getOrDefault("함흥식비빔냉면","No")
        } else {
            //"함흥식비빔냉면"번째값 추출
        }
    }

    //only 가변 HashMap
    fun mutable_HashMap(){
        //- HashMap 은 정의후 put 이나 remove 사용 가능하다.
        //- HashMap 은 Key,Value 형태로 사용된다. (다양한 자료형을 하나의 HashMap 에 담을수 있다, 추출시 Key,Value 형태로 가져온다.
        //- HashMap 은 Key는 중복이 불가하지만, Value 는 중복이 가능하다.
        //- HashMap 은 순서가 보장되지 않는다.(add 순서대로 출력되지 않는다)
        //- iterator 로도 값을 가져 올 수있다.
        var exHashMap = hashMapOf<Any,Any>()
        exHashMap.put("23",0)
        exHashMap.put(0,1)
        exHashMap.put('A',4)

        var itr = exHashMap.iterator()//iterator 사용하여 내부 저장값을 볼 수 있다.(모든 Collection 안에는 iterator 가 존재함)
        while(itr.hasNext()){
            var itrt = itr.next()
            var getKey = itrt.key
            var getValue = itrt.value
            Log.d("mutable_HashMap","getKey : ${getKey}")
            //Log.d("mutable_HashMap","getValue : ${getValue}")
        }


        //Map Key , Value 자료형이 명시적이면 sort기능을 사용 할 수 있다.
        var exHashMap2 = hashMapOf<String,Int>()
        exHashMap.put("첫번째",10)
        exHashMap.put("두번째",20)
        exHashMap.put("세번째",33)

        //value 기준으로 오름차순(ascending) 정렬-1
        val exHashMap_sort_ascending = exHashMap2.toList().sortedBy { (_, value) -> value }.toMap()

        //value 기준으로 오름차순(ascending) 정렬-2
        val exHashMap_sort_ascending_2 = exHashMap2.toList().sortedWith(compareBy { it.second }).toMap()

        //valeu 기준으로 내림차순(descending) 정렬-2
        val exHashMap_sort_descending = exHashMap2.toList().sortedWith(compareByDescending{ it.second }).toMap()
    }

    //only 가변 TreeMap
    fun mutable_TreeMap(){
        //- TreeMap 은 Key,Value 형태로 사용된다. (다양한 자료형을 하나의 HashMap 에 담을수 있다, 추출시 Key,Value 형태로 가져온다.)
        //(단, Any로 key를 지정하였다면, 최초 추가한 자료형이 String 이면 계속 Key type String 만 추가할수있다.)
        //- TreeMap 은 정의후 put 이나 remove 사용 가능하다.
        //- TreeMap 은 Key는 중복이 불가하지만, Value 는 중복이 가능하다.
        //- HashMap 은 순서가 보장 된다.(단, key 값 기준으로 내림차순 정렬된다.)
        //- iterator 로도 값을 가져 올 수있다.
        var exTreeMap = TreeMap<Any,Any>()
        exTreeMap.put("23",0)
        exTreeMap.put("0",0)
        exTreeMap.put("A",4)
        var itr = exTreeMap.iterator()//iterator 사용하여 내부 저장값을 볼 수 있다.(모든 Collection 안에는 iterator 가 존재함)
        while(itr.hasNext()){
            var itrt = itr.next()
            var getKey = itrt.key
            Log.d("mutable_TreeMap","getKey : ${getKey}")
        }
    }

    //only 가변 TreeMap
    fun mutable_LinkedHashMap(){
        //- LinkedHashMap(mutable) 은 Key,Value 형태로 사용된다. (다양한 자료형을 하나의 HashMap 에 담을수 있다, 추출시 Key,Value 형태로 가져온다.)
        //- LinkedHashMap(mutable) 은 Any type 일때, 최초 저장값의 type 을 따르게 된다.
        //- LinkedHashMap(mutable) 은 정의후 put 이나 remove 사용 가능하다.
        //- LinkedHashMap(mutable) 은 Key는 중복이 불가하지만, Value 는 중복이 가능하다.
        //- LinkedHashMap(mutable) 은 순서가 보장 된다.(add 순서대로 출력된다.(내림차순))
        //- iterator 로도 값을 가져 올 수있다.
        var exLinkedHashMap = LinkedHashMap<Any,Any>()
        exLinkedHashMap.put("첫번째다",1)
        exLinkedHashMap.put("두번째다",2)
        exLinkedHashMap.put("세번째다",3)

        var itr = exLinkedHashMap.iterator()//iterator 사용하여 내부 저장값을 볼 수 있다.(모든 Collection 안에는 iterator 가 존재함)
        while(itr.hasNext()){
            var itrt = itr.next()
            var getKey = itrt.key
            Log.d("mutable_LinkedHashMap","getKey : ${getKey}")
        }
    }





    //======== ▼▼ ========= ======== ▼▼ ========= Map 계열 ======== ▼▼ ========= ======== ▼▼ =========
    //불변 Set
    fun immutable_Set(){
        //- Set 은 List 형태로 사용된다.
        //- Set 은 한번 정의되면 add 나 remove 와 같이 변경이 불가능함
        //- Set 은 값의 중복이 불가능 함(중첩되면 최근에 추가된 값이 존재하게 된다)
        //- Set 은 순서가 보장되지 않는다.(add 순서대로 출력되지 않는다)
        //- Set 은 특정 위치의 값을, 지정해서 추출 할 수 없다.
        //- Set 은 iterator를 사용 하여 값을 가져 올 수있다.
        var exSet = setOf<Int>(3,3,1,2)
        Log.d("immutable_Set","exSet : ${exSet}")//중복이 불가하여, 중복은 자동으로 제거된다.
        var itr:Iterator<Int> = exSet.iterator()//iterator 사용하여 내부 저장값을 볼 수 있다.(모든 Collection 안에는 iterator 가 존재함)
        while(itr.hasNext()){
            var itrt = itr.next()
            Log.d("immutable_Set","itrt : ${itrt}")
        }
    }

    //가변 Set
    fun mutable_Set(){
        //- Set(mutable) 은 List 형태로 사용된다.
        //- Set(mutable) 은 정의후 add 이나 remove 사용 가능하다.
        //- Set(mutable) 은 Any type 일때, 최초 저장값의 type 을 따르게 된다.
        //- Set(mutable) 은 값의 중복이 불가능 함(중첩되면 최근에 추가된 값이 존재하게 된다)
        //- Set(mutable) 은 순서가 보장되지 않는다.(add 순서대로 출력되지 않는다)
        //- Set(mutable) 은 특정 위치의 값을, 지정해서 추출 할 수 없다.
        //- Set(mutable) 은 iterator를 사용 하여 값을 가져 올 수있다.
        var exSetMutable = mutableSetOf<Int>(4,4,5,6)
        Log.d("mutable_Set","exSetMutable : ${exSetMutable}")//중복이 불가하여, 중복은 자동으로 제거된다.
        var itr:Iterator<Int> = exSetMutable.iterator()//iterator 사용하여 내부 저장값을 볼 수 있다.(모든 Collection 안에는 iterator 가 존재함)
        while(itr.hasNext()){
            var itrt = itr.next()
            Log.d("mutable_Set","itrt : ${itrt}")
        }

    }

    //가변 HashSet
    fun mutable_HashSet(){
        //- HashSet(mutable) 은 List 형태로 사용된다.
        //- HashSet(mutable) 은 정의후 add 이나 remove 사용 가능하다.
        //- HashSet(mutable) 은 중복이 불가능 함(중첩되면 최근에 추가된 값이 존재하게 된다)
        //- HashSet(mutable) 은 순서가 보장되지 않는다.(add 순서대로 출력되지 않는다)
        //- HashSet(mutable) 은 특정 위치의 값을, 지정해서 추출 할 수 없다.
        //- HashSet(mutable) 은 iterator를 사용 하여 값을 가져 올 수있다.
        var exHashSetMutable = hashSetOf<Any>()
        exHashSetMutable.add("kim")
        exHashSetMutable.add(10000)
        exHashSetMutable.add('B')
        Log.d("mutable_HashSet","exHashSetMutable : ${exHashSetMutable}")//중복이 불가하여, 중복은 자동으로 제거된다.
        var itr:Iterator<Any> = exHashSetMutable.iterator()//iterator 사용하여 내부 저장값을 볼 수 있다.(모든 Collection 안에는 iterator 가 존재함)
        while(itr.hasNext()){
            var itrt = itr.next()
            Log.d("mutable_HashSet","itrt : ${itrt}")
        }
    }

    //가변 TreeSet
    fun mutable_TreeSet(){
        //- TreeSet(mutable) 은 List 형태로 사용된다.
        //- TreeSet(mutable) 은 정의후 add 이나 remove 사용 가능하다.
        //- TreeSet(mutable) 은 Any type 일때, 최초 저장값의 type 을 따르게 된다.
        //- TreeSet(mutable) 은 중복이 불가능 함(중첩되면 최근에 추가된 값이 존재하게 된다)
        //- TreeSet(mutable) 은 순서가 보장 된다.(add 순서의 역순으로 출력된다.(오름차순))
        //- TreeSet(mutable) 은 특정 위치의 값을, 지정해서 추출 할 수 없다.
        //- TreeSet(mutable) 은 iterator를 사용 하여 값을 가져 올 수있다.
        var exTreeSetMutable = TreeSet<Any>()
        exTreeSetMutable.add("첫번째 값")
        exTreeSetMutable.add("2")
        exTreeSetMutable.add("C")
        Log.d("mutable_TreeSet","exTreeSetMutable : ${exTreeSetMutable}")//중복이 불가하여, 중복은 자동으로 제거된다.
        var itr:Iterator<Any> = exTreeSetMutable.iterator()//iterator 사용하여 내부 저장값을 볼 수 있다.(모든 Collection 안에는 iterator 가 존재함)
        while(itr.hasNext()){
            var itrt = itr.next()
            Log.d("mutable_TreeSet","itrt : ${itrt}")
        }
    }

    //가변 LinkedHashSet
    fun mutable_LinkedHashSet(){
        //- LinkedHashSet(mutable) 은 List 형태로 사용된다.
        //- LinkedHashSet(mutable) 은 정의후 add 이나 remove 사용 가능하다.
        //- LinkedHashSet(mutable) 은 중복이 불가능 함(중첩되면 최근에 추가된 값이 존재하게 된다)
        //- LinkedHashSet(mutable) 은 순서가 보장 된다.(add 순서대로 출력된다.(내림차순))
        //- LinkedHashSet(mutable) 은 특정 위치의 값을, 지정해서 추출 할 수 없다.
        //- LinkedHashSet(mutable) 은 iterator를 사용 하여 값을 가져 올 수있다.
        var exLinkedHashSetMutable = linkedSetOf<Any>()
        exLinkedHashSetMutable.add("첫번째 값")
        exLinkedHashSetMutable.add(2)
        exLinkedHashSetMutable.add('C')
        Log.d("mutable_LinkedHashSet","exLinkedHashSetMutable : ${exLinkedHashSetMutable}")//중복이 불가하여, 중복은 자동으로 제거된다.
        var itr:Iterator<Any> = exLinkedHashSetMutable.iterator()//iterator 사용하여 내부 저장값을 볼 수 있다.(모든 Collection 안에는 iterator 가 존재함)
        while(itr.hasNext()){
            var itrt = itr.next()
            Log.d("mutable_LinkedHashSet","itrt : ${itrt}")
        }
    }

    //가변 SortedSet
    fun mutable_SortedSet(){
        //- SortedSet(mutable) 은 List 형태로 사용된다.
        //- SortedSet(mutable) 은 정의후 add 이나 remove 사용 가능하다.
        //- SortedSet(mutable) 은 Any type 일때, 최초 저장값의 type 을 따르게 된다.
        //- SortedSet(mutable) 은 중복이 불가능 함(중첩되면 최근에 추가된 값이 존재하게 된다)
        //- SortedSet(mutable) 은 순서가 보장 된다.(add 순서로 출력된다.(내림차순))
        //- SortedSet(mutable) 은 특정 위치의 값을, 지정해서 추출 할 수 없다.
        //- SortedSet(mutable) 은 iterator를 사용 하여 값을 가져 올 수있다.
        var exSortedSetMutable = sortedSetOf("s","0","dd","s")
        Log.d("mutable_SortedSet","exSortedSetMutable : ${exSortedSetMutable}")//중복이 불가하여, 중복은 자동으로 제거된다.
        var itr:Iterator<Any> = exSortedSetMutable.iterator()//iterator 사용하여 내부 저장값을 볼 수 있다.(모든 Collection 안에는 iterator 가 존재함)
        while(itr.hasNext()){
            var itrt = itr.next()
            Log.d("mutable_SortedSet","itrt : ${itrt}")
        }
    }



/*
<Iterator 사용>
    HashMap<Integer,String> map = new HashMap<Integer,String>(){{//초기값 지정
        put(1,"사과");
        put(2,"바나나");
        put(3,"포도");
    }};

//entrySet().iterator()
    Iterator<Entry<Integer, String>> entries = map.entrySet().iterator();
    while(entries.hasNext()){
        Map.Entry<Integer, String> entry = entries.next();
        System.out.println("[Key]:" + entry.getKey() + " [Value]:" +  entry.getValue());
    }
//[Key]:1 [Value]:사과
//[Key]:2 [Value]:바나나
//[Key]:3 [Value]:포도

//keySet().iterator()
    Iterator<Integer> keys = map.keySet().iterator();
    while(keys.hasNext()){
        int key = keys.next();
        System.out.println("[Key]:" + key + " [Value]:" +  map.get(key));
    }
//[Key]:1 [Value]:사과
//[Key]:2 [Value]:바나나
//[Key]:3 [Value]:포도

*/

}//class end