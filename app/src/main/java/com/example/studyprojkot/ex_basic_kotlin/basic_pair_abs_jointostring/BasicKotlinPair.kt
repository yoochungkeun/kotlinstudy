package com.example.studyprojkot.ex_basic_kotlin.basic_pair_abs_jointostring

import android.os.Bundle
import android.util.Log
import com.example.studyprojkot.BaseActivity
import kotlin.math.abs

class BasicKotlinPair:BaseActivity() {

    //Pair : map과 비슷하게 생겼지만, map은 <Key,Value> / pair 는 <Value,Value> 형태이다.
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ex01()
        ex02()
    }

    //map + pair 혼용
    fun ex01(){
        var testPair = Pair<Int,Int>(10,1)
        val testMap = mapOf(
                                                  0 to Pair(1,3),
                                                  1 to Pair(0,0)
                                                  )
        //map 은 기존과 같이 key 로 해당 값을 추출 하고, pair 는 first, second 로 두개의 값을 추출한다.
        Log.d("test123","${testMap.get(1)}")
        //출력=> (0,0)

        Log.d("test123","${testMap.get(0)?.first}")
        //출력=> 1

        Log.d("test123","${testMap.get(0)?.second}")
        //출력=> 3

        //일반적인 minus 기호로 계산하면
        var tt2:Double = (testMap[0]!!.first - testPair.first).toDouble()
        Log.d("test123","tt2 : ${tt2}")
        //출력=> -9.0

        //asb 를 이용한 절대값 계산하면
        var tt:Double = abs(testMap[0]!!.first - testPair.first).toDouble()
        Log.d("test123","tt : ${tt}")
        //출력=> 9.0
    }

    //joinToString
    fun ex02(){
        val str = "B2BFK9D0S009EJK6"
        val mI2 = str.toMutableList()
        Log.d("test123","mI2 : ${mI2}")
        Log.d("test123","mI2.joinToString : ${mI2.joinToString("-")}")
    }


}//class end