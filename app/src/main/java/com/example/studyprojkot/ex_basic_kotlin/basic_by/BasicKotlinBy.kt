package com.example.studyprojkot.ex_basic_kotlin.basic_by

import android.os.Bundle
import android.util.Log
import com.example.studyprojkot.BaseActivity

class BasicKotlinBy: BaseActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val d = BaseImpl()

        //d.print()//Derived_2 에 있는 print() 출력하는 방법
        SumClass(d).result()//1.Derived_2 의 인스턴스를 Derived 에 전달한다.
    }

    //2. 전달 받은 BaseImpl 의 인스턴스를 by 를 이용하여 interface Base에 e를 위임한다.
    //! 위와 같이 by를 사용해서 interface를 위임하지 않을 경우, Derived 현재 Base를 구현하고 있으므로 Base에 있는 print()를 override를 해야한다.
    //이렇게 되면 같은 함수를 반복해서 구현해야하고, 기존의 구현된 함수와 헷갈릴수도 있으므로, override 하지 않고, 사용 할 수 있게 by를 써준거다.
    class SumClass(e:Base):Base by e{
        fun result(){
            myminus(10,10)
        }
    }

    class BaseImpl():Base{
        override fun myplus(num1: Int, num2: Int): Int {
            return num1+num2
        }
        override fun myminus(num1: Int, num2: Int): Int {
            return num1-num2
        }
    }

    interface Base{
        fun myplus(num1: Int,num2:Int):Int
        fun myminus(num1: Int,num2:Int):Int
    }


}//class end