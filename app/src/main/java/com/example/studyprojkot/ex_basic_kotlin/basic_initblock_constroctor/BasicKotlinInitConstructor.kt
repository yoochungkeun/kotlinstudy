package com.example.studyprojkot.ex_basic_kotlin.basic_initblock_constroctor



/*
Constructor 전달인자값 호출은 최우선순위이고, 나머지 init 포함한 기타 변수함수는 위에서 아래로 순서대로 호출된다.
init 은 별도로 호출하지 않아도, 해당 클래스가 생성되면 위에서 아래 순서로 모두 호출된다. 일반 함수는 생성될때 호출되지는 않는다.
*/

open class Parent{

    private val a = println("Parent.a - #3")

    constructor(arg: Unit= println("Parent constructor - #2")){
        println("Parent constructor block - #6")
    }

    init{
        println("Parent.init - #4")
    }

    private val b = println("Parent.b - #5")

}

//Launcher activity 되는 activity 에서 BasicKotlinInitConstructor 를 생성만 해줘도 확인가능함
class BasicKotlinInitConstructor : Parent{
    val a = println("Child.a - #7")

    init{
        println("Child.init - #8")
    }

    constructor(arg: Unit= println("Child constructor arg - #1")):super(){
        println("Child constructor arg block - #11")
    }

    val b = println("Child.b - #9")

    constructor(arg: Int, arg2:Unit= println("Child constructor arg1,2")):this(){
        println("Child constructor arg1,2 block ")
    }

    init {
        println("Child.init - #10")
    }

}





