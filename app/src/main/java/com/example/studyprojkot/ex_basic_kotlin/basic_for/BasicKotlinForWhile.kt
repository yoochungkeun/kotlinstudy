package kr.co.newlinkcorp.studyprojkotlin.ex_basic_kotlin.basic_for

import android.os.Bundle
import android.util.Log
import com.example.studyprojkot.BaseActivity

class BasicKotlinForWhile : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //forFunc_6()
        dowhileFunc()
    }

    //◈1. For
    fun forFunc_1() {
        // i 는 1 부터 10까지 1씩 증가하기
        for (i in 1..10) {
            Log.d("yck", "forFunc_1 : ${i}")
        }
    }

    //◈1-2. For
    fun forFunc_2() {
        // i 는 1 부터 10까지 2씩 증가하기
        for (i in 1..10 step 2) {
            Log.d("yck", "forFunc_2 : ${i}")
        }
    }

    //◈1-3. For
    fun forFunc_3() {
        // i 는 10 부터 1까지 1씩 감소하기
        for (i in 10 downTo 1) {
            Log.d("yck", "forFunc_3 : ${i}")
        }
    }

    //◈1-4. For
    fun forFunc_4() {
        // i 는 10 부터 1까지 2씩 감소하기
        for (i in 10 downTo 1 step 2) {
            Log.d("yck", "forFunc_4 : ${i}")
        }
    }

    //◈1-5. For
    fun forFunc_5() {
        // i 는 1 부터 9까지 1씩 증가하기(until은 마지막숫자를 포함시키지않음)
        for (i in 1 until 10) {
            Log.d("yck", "forFunc_5 : ${i}")
        }
    }

    //◈1-6. For
    fun forFunc_6() {
        // i 는 list 크기 만큼 반복한다.
        val list = listOf<Int>(10, 20, 30, 40, 50)
        for (i in list) {
            Log.d("yck", "forFunc_6 : ${i}")
            // indices 로 사용 하지 않고for 돌리고, 아래와 같이 list의 값을 직접 접근하면 에러가 발생한다.
            //Log.d("yck","forFunc_6 : ${list[i]}")
        }
    }

    //◈1-7. For
    fun forFunc_7() {
        // i 는 list 크기 만큼 반복한다.(위에서 와는 다르게 )
        val list = listOf<Int>(10, 20, 30, 40, 50)
        for (i in list.indices) {
            Log.d("yck", "forFunc_7 : ${list[i]}")
        }
    }


    //◈2. While
    fun whileFunc() {
        var x = 1
        var sum = 0
        while (x <= 10) {
            Log.d("yck", "whileFunc x : ${x}")
            sum += x
            x++
        }
    }


    //◈3. do While
    fun dowhileFunc() {
        var x = 1
        var sum = 0
        //
        do {
            Log.d("yck", "dowhileFunc x : ${x}")
            sum += x
            x++
        } while (x <= 10)
            Log.d("yck", "dowhileFunc sum : ${sum}")
    }


}//class end