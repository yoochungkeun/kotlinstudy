package com.example.studyprojkot.ex_basic_kotlin.basic_listener

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import com.example.studyprojkot.BaseActivity
import com.example.studyprojkot.R

class BasicKotlinListener : BaseActivity(),View.OnClickListener{

    /*방법-1*/lateinit var btn_1:Button
    /*방법-2*/lateinit var btn_2:Button
    /*방법-3*/lateinit var btn_3:Button
    /*방법-4*/lateinit var btn_4:Button
    /*방법-5*/lateinit var btn_5:Button
    /*방법-6*/lateinit var btn_6:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_basic_listener)

        /*방법-1*/btn_1= findViewById(R.id.btn)
        /*방법-1*/btn_1.setOnClickListener(this)

        /*방법-2*/btn_2= findViewById(R.id.btn)
        /*방법-2*/btn_2.setOnClickListener(clickListener)

        /*방법-3*/btn_3= findViewById(R.id.btn)
        btn_3.setOnClickListener(View.OnClickListener {
            Log.d("yck","OnClick_3")
        })

        /*방법-4*/btn_4 = findViewById(R.id.btn)
        /*방법-4*/btn_4.setOnClickListener(clickListener2)

        /*방법-5*/btn_5 = findViewById(R.id.btn)
        /*방법-5*/btn_5.setOnClickListener(::onClicked)

        /*방법-6*/btn_6.setOnClickListener {
            view ->
            //do something
        }


    }

    /*방법-1*/ //클래스에 View.OnClickListener 상속 받으면 implement 시켜서 해당 리스너 this 로 연결해서 사용하는 방법
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.tv ->{
                Log.d("yck","OnClick_1")
            }
        }
    }

    /*방법-2*/
    val clickListener :View.OnClickListener = object :View.OnClickListener{
        override fun onClick(v: View?) {
            when(v?.id){
                R.id.tv ->{
                    Log.d("yck","OnClick_2")
                }
            }
        }
    }

    /*방법-4*/ //람다식 사용
    val clickListener2 :View.OnClickListener = View.OnClickListener { view ->
        when(view.id){
            R.id.tv ->{
                Log.d("yck","OnClick_3")
            }
        }
    }

    /*방법-5*/
    fun onClicked(view:View){
        when(view.id){
            R.id.tv ->{
                Log.d("yck","OnClick_3")
            }
        }
    }



}//class end