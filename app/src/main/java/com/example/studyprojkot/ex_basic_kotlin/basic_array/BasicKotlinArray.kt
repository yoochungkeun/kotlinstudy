package com.example.studyprojkot.ex_basic_kotlin.basic_array

import android.os.Bundle
import android.util.Log
import com.example.studyprojkot.BaseActivity
import com.example.studyprojkot.R


class BasicKotlinArray : BaseActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mapFunc()
    }

    //◈1. Array
    fun arrayFunc(){
        /*array생성방법-1*/
        var array = arrayOf(1,2,3,4,5)
        /*array생성방법-2*/
        var arrayInt = arrayOf<Int>(1,2,3,4,5)

        /*array생성방법-3(빈 Array 생성*/
        var arrayInt_2 = emptyArray<Int>()

        array[0]=6
        array[1]=7
        Log.d("yck","arrayFunc/array0: ${array[0]}")
        Log.d("yck","arrayFunc/array1: ${array[1]}")

        /*array생성방법-3*/
        //기초타입과 관련해서 아래와 같이 배열을 선언 할 수 도있다.
        var arrayInt2 = intArrayOf(1,2,3,4,5)//int Array
        var arrayChar = charArrayOf('A','B','C')//Char Array
        var arrayLonng = longArrayOf(1L,2L,3L,4L,5L)//Long Array
        var arrayBoolean = booleanArrayOf(true,false,true)//booleanS Array

        /*array생성방법-4*/
        //array size 로 생설 할 때
        var array3 = arrayOfNulls<Any>(3)
        array3[0] = 100
        array3[1] = "hello"
        array3[2] = "world"
        Log.d("yck","arrayFunc/array3: ${array[0]}${array[1]}${array[2]}")
        //배열의 초깃값을 빈상태("")로
        var emptyArray = Array<String>(3,{""})
    }


    //◈2. List
    fun listSetMapFunc(){
        // List listof() 불변 list 계의 final 같은 존재
        var list:List<String> = listOf("Hello","world")

        // MutableList 가변, JAVA 에서 list ArrayList 와 비슷
        var mulist:MutableList<String> = mutableListOf("Hello","world")
        mulist.add(0,"hello2")
        mulist.set(0,"hello2")
        mulist.removeAt(0)

        //Arraylist
        var arrayList:ArrayList<String> = ArrayList()
        arrayList.add(0,"hello3")
        arrayList.set(0,"hello3")
    }

    //◈3. map
    fun mapFunc(){
        //Map-Pair 선언하는 방법 (mapOf 불변)
        var map = mapOf<String,String>(Pair("1","hello"), Pair("2","world"), Pair("10","world10"))
        Log.d("yck","mapFunc/map: ${map.get("10")}")

        //Map-to 선언하는 방법 (mapOf 불변)
        var mapTo = mapOf(1 to "hello",2 to "world",20 to "world20")
        Log.d("yck","mapFunc/mapTo: ${mapTo.get(20)}")

        //MutableMapOf 선언하는 방법 (mutableMapOf 가변)
        var mutableMap = mutableMapOf<String,String>()
        mutableMap.put("1","hello")
        mutableMap.put("21","hello")
    }

    //◈4. map (JAVA 에서 사용하던 ArrayList , HashMap , HashSet 사용가능)
    fun setFunc(){
        var list: ArrayList<String> = ArrayList()
        list.add("xxx")

        var hashMap:HashMap<String,String> = HashMap()
        hashMap.put("1","A")

        var set:HashSet<String> = HashSet()
        set.add("B")
    }

    fun multiArrayFunc(){
        val multiArr = arrayOf(arrayOf(1,2,3), arrayOf("야","어","여"))
        multiArr.get(0).get(0) //= 1
        multiArr.get(0).get(1) //= "야"
        multiArr.get(1).get(0) //= 2
        multiArr.get(1).get(1) //= "어"
        multiArr.get(2).get(0) //= 3
        multiArr.get(2).get(1) //= "여"
    }
    
    



}//class end