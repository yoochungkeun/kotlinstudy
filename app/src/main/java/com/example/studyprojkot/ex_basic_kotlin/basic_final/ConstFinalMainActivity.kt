package com.example.studyprojkot.ex_basic_kotlin.basic_final

import android.os.Bundle
import com.example.studyprojkot.BaseActivity


class ConstFinalMainActivity: BaseActivity() {

    companion object{
        const val NAME:String = "홍길동"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //val 이 final 과 같지 않은 이유는
        //일단 단순하게 직접 변경하면, 변경불가 에러가 나타난다. 이부분은 java 의 상수 과 같다.
            //var test = Test()
            //test.name = "홍길동 아니다."  => Error
        //근데, field를 이용하여 내부에서 변경 시도 하면, 변경이 가능하게 된다.
            //var test2 = Test2()
            //test2.name => "홍길동 아니다" => Success
        //이럴때 사용하는것이 const 이다. const 를 앞에 붙여주면 상수와 같은 기능을 하게 된다.
        //단, 오브젝트(클래스)/함수에 사용할수 없어서 보통 companion object{ } 안에 사용한다.
    }

    class Test{
        val name:String = "홍길동"
    }

    class Test2{
        val name:String ="홍길동"
        get() {
            return field+"아니다."
        }
    }


}//class end