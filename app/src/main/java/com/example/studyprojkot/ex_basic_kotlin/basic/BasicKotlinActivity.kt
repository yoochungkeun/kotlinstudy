package com.example.studyprojkot.ex_basic_kotlin.basic

import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.widget.LinearLayout
import com.example.studyprojkot.BaseActivity
import java.lang.StringBuilder


// open class BasicKotlinActivity:CommonActivity() {  }
// public class BasicKotlinActivity:CommonActivity() {  } in JAVA
class BasicKotlinActivity : BaseActivity() {

    //◈ 1. 프로퍼티 선언(자료형)
    //코틀린에서는 클래스의 val / var 로 정의 되어있는 변수를 일컬어 프로퍼티라고 한다.
    //즉, 필드와(필드변수==전역변수) 접근자(get(),set())을 통칭하는 단어 이다.

    //기본 선언
    var countint: Int = 40 //Int type
    var countStr: String? = null//String type
    var countDouble: Double = 0.0//Double type
    var countLong: Long = 0L//Long type
    val diceRange = 1..6 // 1~6까지 숫자를 담는 intRange type

    var value: Int = 10 //가변형 프로퍼티 선언
    val value2: String = "도시" //읽기 전용 프로퍼티 선언
    var value3 = "String자동변환"//타입생략 선언(유형추론방식)

    //in JAVA : final String countFinal = "자바에서파이널";
    val countFinal: String = "val은 자바에서파이널"

    //? 코틀린에서는 Null 에 예민하기 때문에 자료형 뒤에 ? 붙여서 해당 프로퍼티에 null 이 들어 갈 수도 있다고, 따로 표시해 주어야한다.
    val languageName: String? = null // 정상

    //static in kotlin
    companion object {
        var st_valule: String = "자바에서스태틱 static"
        //in JAVA
        //static String st_value = "자바에서스태틱";
    }

    //코틀린에서는 전역변수(멤버변수)도 초기화 해주어야 한다. 이때 변수만 만들고, 나중에 초기화 해주겠다고 표시해주는 것이 lateinit 이다.
    //var 만 사용 가능하며, lateinit 사용한 프로퍼티는 체크하는 코드가 따로 있다.
    /*
    //아래와 같이 isInitialized 를 사용하여 초기화를 체크해 주어야 한다. lateInitString == null 을쓰면 에러난다.
    if(::lateInitString.isInitialized){

    }
    */
    //var lateInitString:String => 이렇게 적으면 에러난다.
    lateinit var lateInitString: String



    //위의 lateinit 은 var 같은 가변타입 변수에 사용되고, val 처럼 불변 타입은 아래와 같이 초기화 시켜주어야 한다.
    val lazyInitString by lazy {
        "초기화한다."
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var test2 = testFunction2(30)
        Log.d("yck", "test2 Up and Dowm : " + test2)

        var test3 = testFuntion3("안드로이드")
        Log.d("yck", "test3 입력된 문자열 길이 : " + test3)

        var stringMapper = stringMapper("안드로이드", { inputck ->
            inputck.length
        })

        diceRange.random()//1~6사이의 값중 랜덤으로 출력
        testFuntion3("Android")//익명함수
        elvis()//엘비스연산자
        ifElse()//if-else연산자s
        letFunc()//let()
        runFunc()//run()
        alsoFunc()//also()

        //최대 값 산출
        Log.d("yck"," maxof = ${maxOf(1,5)}")
        //최소 값 산출
        Log.d("yck"," maxof = ${minOf(1,5)}")

        Log.d("yck"," specialFunc = ${specialFunc()}")
    }

    //◈ 2. 함수 사용법(단순화)
    fun testFunction(count: Int): String {
        return if (count > 20) {
            "Up to 20"
        } else {
            "Down to 20"
        }
    }

    //◈ 2-1. 함수 사용법(단순화)
    fun testFunctionUnit(count: Int): Unit {
        //return type 을 Unit 이라고 한건, void 와 같은 의미이다. 이는 생략 가능하다.
    }

    //◈ 2-2. 함수 사용법(반환키워드를 할당 연산자 바꿈)
    //문자열입력 ▶ 20 위아래 판단값 반환
    fun testFunction2(count: Int): String = if (count > countint) {
        "Up to " + countint
    } else {
        "Down to " + countint
    }

    //◈ 2-3. 함수 사용법(익명함수)
    //문자열입력 ▶ 문자열길이 반환
    val testFuntion3: (String) -> Int = { value ->
        //받은값(String)을 반환시, Int 로 하겠다는 코드임
        value.length
    }

    //◈ 2-4. 함수 사용법(고차함수)
    fun stringMapper(str: String, mapper: (String) -> Int): Int {
        return mapper(str)
    }

    //◈ 2-5. 함수 사용법(special함수?)
    var a = 10
    var b = 20
    fun specialFunc() = a+b

    //계산식을 함수에 바로 참조 시킬수도 있다.
    //위의 specialFunc 를 호출하면 결과값은 30이 된다.

    //◈ 3. 어션셜 연산자(!!) (Null값 보증한다는 의미)
    fun asstiontial() {
        val animal = "호랑이"
        val animalName: String = animal!! //animalName 은 절대 Null 아니다 라고 보증함, 단 null 발생시 npe 발생
    }

    //◈ 4. 엘비스 연산자(?:) (Null값에 대비한 defualt 셋팅)
    fun elvis() {
        var first_name: String = ""
        val name: String = countStr ?: "count Str is null"
        //삼항연산자와같이 countStr 이 null 일경우에, name 에는 no firstName 이 들어가게 된다.    }
        class elvisNames {
            var name: String? = null
        }
        Log.d("yck", "name : ${name}")
    }

    //◈ 5. if-else if
    fun ifElse() {
        var count_ifelse = 30
        //방법-1
        if (count_ifelse == 42) {
            Log.d("yck", "I have a answer1")
        } else if (count_ifelse > 35) {
            Log.d("yck", "The answer is close1")
        } else {
            Log.d("yck", "The answer eludes me.1")
        }

        //방법-2
        val answerString: String = if (count_ifelse == 42) {
            "I have the answer.2"
        } else if (count_ifelse > 35) {
            "The answer is close.2"
        } else {
            "The answer eludes me.2"
        }

        Log.d("yck", "${answerString}")
    }

    //◈ 5. when ( switch in JAVA)
    fun whenFunc() {
        var valueWhen = 10
/*
when 적용 전 코드
        var result = ""
        if(valueWhen==42){
            result = "I have the answer"
        }else if(valueWhen>35){
            result = "The answer is close"
        }else{
            result = "The answer eludes me"
        }
        Log.d("yck","${result}")
*/
//적용된 코드-1
        val result = when {
            valueWhen == 42 -> "I have the answer"
            valueWhen > 35 -> "The answer is close."
            else -> "The answer eludes me."
        }
        Log.d("yck", "${result}")
    }

    // Activity , Context 에서 사용
    //◈ 6. apply()  => Scorpe Funtion : 객체를 사용할때 범위,영역을 일시적으로 만들어서 속성이나 함수를 처리하는 용도로 사용됨
    //▶ apply 는 담으려는 변수를 연결하여, 블록 내부에 연결하여, 직접 속성을 지정 할 수 있다.
    fun applyFunc() {
/*
apply()적용전
        val param = LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.WRAsP_CONTENT)
        param.gravity = Gravity.CENTER_HORIZONTAL
        param.weight = 1f
        param.topMargin = 100
        param.bottomMargin = 100
*/
        //적용-1
        val param = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT).apply {
            gravity = Gravity.CENTER_HORIZONTAL
            weight = 1f
            topMargin = 100
            bottomMargin = 100
        }
    }

    // Activity , Context 에서 사용
    //◈ 6. let() => Scorpe Funtion : 객체를 사용할때 범위,영역을 일시적으로 만들어서 속성이나 함수를 처리하는 용도로 사용됨
    //▶ let 은 별도의 변수 선언 필요없이, 블록내부로 연결하여 내부의 함수에 전송하여 사용 할 수 있다.
    fun letFunc() {
/*
let()적용전 코드
        val padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16f, resources.displayMetrics).toInt()
        setPadding(padding,0,0,padding)
*/
        /* 적용된 코드-1
        TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,16f,resources.displayMetrics).toInt().let {it
            setPadding(it,0,it,0)
        }
        */
        //적용된 코드-2
        TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16f, resources.displayMetrics)
            .toInt().let { value ->
                setPadding(value, 0, value, 0)
            }
    }
    fun setPadding(padding: Int, margin: Int, padding2: Int, margin2: Int) {
        //패딩
        Log.d(
            "yck",
            "padding :${padding}, margin:${margin}, padding2:${padding2}, margin2:${margin2}"
        )
    }

    // Activity , Context 에서 사용
    //◈ 7. run() => Scorpe Funtion : 객체를 사용할때 범위,영역을 일시적으로 만들어서 속성이나 함수를 처리하는 용도로 사용됨
    //▶ apply()와 유사하지만, apply()는 새로운 객체생성과 동시에 연속된 작업이 필요할때 사용하고,
    //run()은 이미 생성된 객체에 연속된 작업이 필요 할 때 사용한다.+
    fun runFunc() {
        var test: String? = null
        test.run {
            Log.d("yck", "test is not null")
            // 만약 전역 변수로 Context를 받아서 run을 쓴다면
            /*
            class TestRun constructor(context:Context?) {
                val test2 = context?.run{
                    Test2(this)
                }
            }
            ....
            //예를들어 다른 클래스(Test2 class) 객체생성시 context 가 필요한 상황이라면
            //위와같이 this 를 바로 context 에서 받아서 사용할 수있다.
            // 그외의 값을(String ,int 등등) 을 받으려면 let을 사용하여 it 으로 받는것이 좋다.
            */
            return
        }
        /*
        var test2 = "123".apply {
            runFuncSub(length)
        }
        */
    }
    fun runFuncSub(getLength: Int) {
        Log.d("yck", "getLength:${getLength}")
    }

    // Activity , Context 에서 사용
    //◈ 8. also() => Scorpe Funtion : 객체를 사용할때 범위,영역을 일시적으로 만들어서 속성이나 함수를 처리하는 용도로 사용됨
    //▶ it 을 사용해 프로퍼티에 접근하므로, 주로 로그를 찍을때 사용 하면 좋다.
    fun alsoFunc() {
        //val numbers = arrayListOf<String>("one","two","three")
        val numbers = arrayListOf<Int>(1, 2, 3)
        numbers.also {
            Log.d("yck", "alsoFunc numbers: ${it}")
        }
    }

    //◈ 9. with() => Scorpe Funtion : 객체를 사용할때 범위,영역을 일시적으로 만들어서 속성이나 함수를 처리하는 용도로 사용됨
    //▶ 객체 이름을 반복하지 않고 사용하고 싶을때, with 함수를 사용하면 편하다
    fun widthFunc() {
/*
적용 전
        val stringBuilder = StringBuilder()
        for(letter in 'A'..'Z'){
            stringBuilder.append(letter)
        }
        toString()
*/
        //적용-1
        //StringBuilder 객체가 들어갔기 때문에 아래의 코드는 StringBuilder()인스턴스가 존재하는 것이기 때문에
        //StringBuilder.append 사용하는 방식처럼 블록 안에 전체가 StringBuilder 인스턴스라고 생각하고 사용하면 된다.
        val text = with(StringBuilder()) {
            for (letter in 'A'..'Z') {
                append(letter)
            }
        }
    }


    //◈ 10. "is" 는 JAVA 에서 instanceof 와 같은 역할을 한다, "Any" 는 JAVA 에서 Object 와 같은 역할을 한다.
    fun isAnyFunc(value: Any): Boolean {
        if (value is String) {
            return true
        }
        return false
    }


    //◈ 11. 컬렉션 타입(Array,Map,List,Set)
    //BasicKotlinArray.kt


    //◈ 12. object 함수
    fun objectFunc() {
        val person = object {
            val name: String = "홍길동"
            val age: Int = 30
        }
        Log.d("yck", "person${person.name}")
        Log.d("yck", "person${person.age}")
    }

    //◈ 13. get()set
    fun getSetFunc() {
        val person = object {
            var name: String = "홍길동"
                get() {//getter
                    return field
                }
                set(value) {//setter
                    //value data
                }
            var age: Int = 30
                get() {//getter
                    return field
                }
                set(value) {//setter
                    //value data
                }
        }
    }


    //◈ 14. Object Class
    object Person2 {
        var name: String = ""
        var age: Int = 0
        fun print() {
            Log.d("yck","Person2 object class name : ${name}")
            Log.d("yck","Person2 object class age: ${age}")
        }
    }


    //◈ 15. by (위임하기)
    //BasicByActivity.kt


    //◈ 17. onClickListener (클릭리스너)
    //BasicKotlinListener.kt


    //◈ 18. For while (반복문)
    //BasicKotlinFor.kt


    //◈ 19. class (클래스)
    //BasicKotlinClass.kt






}//class end