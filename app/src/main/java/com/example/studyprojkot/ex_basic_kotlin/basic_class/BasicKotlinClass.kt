package kr.co.newlinkcorp.studyprojkotlin.ex_basic_kotlin.basic_class

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.studyprojkot.BaseActivity


class BasicKotlinClass() : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

/*
        //◈ 3-3. Data Class
        val testDataClass = BasicDataClass("호랑이", "동물", 2)
        Log.d("yck", "testDataClass.name : ${testDataClass.name}")

        //◈ 4-1. Object Class
        BasicObjectClass.name = "호랑이"
        BasicObjectClass.type = "동물"
        BasicObjectClass.num = 2
        BasicObjectClass.myinfo()

        //◈ 5. Enum Class
        //해당 Enum 클래스의 값을 프로퍼티에 담아 호출할때 name 은 해당 값을 직접 가져오는 것이고, ordinal 은 해당 값의 위치값을 가져오는 것이다.
        val enumvalue = BasickEnumClass.TIGER
        Log.d("yck","enum name : ${enumvalue.name}")
        Log.d("yck","enum ordinal : ${enumvalue.ordinal}")

        //◈ 5-1. Enum Class (array 이용)
        //아래와 같이 Array 에 담아서 다른 방법으로 처리하는 방법 도 있음
        //EnumClass.values() => 해당 EnumClass 의 값을 통째로 Array에 넣는다.
        val enumArrayValue:Array<BasickEnumClass> = BasickEnumClass.values()
        for(i in 0..enumArrayValue.size-1){
            Log.d("yck","enumArrayValue[${i}] : ${enumArrayValue[i]}")
        }

        val enumvalue2 = BasicEnumClass2.TIGER
        Log.d("yck","enumvalue2.num : ${enumvalue2.level}")
        Log.d("yck","enumvalue2.type : ${enumvalue2.type}")
*/

/*
        //◈ 2-3. class constructor
        var constClass: constructorClass2 = constructorClass2(this)
        constClass.showToast()
*/

        //◈ 7-2. 추상 클래스
        var abstractClass = abstractMainClass()
        abstractClass.custom()

    }


//in JAVA
/*
public class JavaClass {
    String name;
    JavaClass(String name){
        this.name = name;
    }
    //...
}

*/
//◈ 1. class init : 코틀린은 java 처럼 생성자를 만들수 없다. 이때, 코틀린은 init 을 사용한다.
//방법-1
/*
class BasicKotlinSubClass(context: Context) {
    var name:String? = null
    init {
        this.name = name
    }
    //...
}
*/

//◈ 1-1. class init
//아래와 같이 init 을 하지 않고 바로 매개변수를 사용 할 수도 있다.
/*
class BasicKotlinSubClass(name: Context){
    var name:String = ""
    fun myInfo(){
        this.name = name
    }
    //...
}
*/

//◈ 2. class constructor
//constructor 는 최소 1개 이상의 매개변수를 가지고 있어야함
/*
class BasicKotlinSubClass(){
    var name:String=""
    constructor(name: String):this(){

    }
    //...
}
*/

//◈ 2-1. class constructor
//매개변수 받아서, 클래스에서 contructor 바로 연결하여 init 하는 방법
/*
class BasicKotlinSubClass constructor(context: Context){
    var context:Context?=null
    init {
        this.context = context
    }
    //...
}
*/

    //◈ 2-2. class constructor
    class BasicKotlinSubClass(val context: Context) {
        //val context => 주생성자
        var name: String = ""
        var age: Int = 0

        constructor(context2: Context, name: String) : this(context2) {
            //constructor(context,name) => 부생성자
        }

        constructor(context3: Context, name: String, age: Int) : this(context3, name) {
            //context,name => 부생성자
        }

        constructor(context4: Context, name: String, age: Int, nick: String) : this(
            context4,
            name,
            age
        ) {
            //context,name => 부생성자
        }
        //위처럼 주생성자가 존재한다면, 부생성자는 무조건 직간접적으로 this(context)를 통해 위임하여야한다.
    }

    //◈ 2-3. class constructor //constructor 를 따로 분리해서 사용
    class constructorClass() {
        var constructor_context: Context? = null

        constructor(context: Context) : this() {
            this.constructor_context = context
        }

        fun showToast() {
            constructor_context
            Toast.makeText(constructor_context, "Test Constructor Class", Toast.LENGTH_SHORT).show()
        }
    }

    //◈ 2-4. class constructor  // constructor를 클래스에 바로 적용해서 사용
    class constructorClass2 constructor(context: Context) {
        var constructor_context2 = context
        fun showToast() {
            constructor_context2
            Toast.makeText(constructor_context2, "Test Constructor Class22", Toast.LENGTH_SHORT)
                .show()
        }
    }


    //◈ 3. Data Class
    data class BasicDataClass(
        val name: String,
        val type: String,
        val num: Int
    )


    //◈ 4. Object Class
    // 특징은 JAVA 에서 static class 와 같은 특징을 띈다.
    // 전역에서 객체화 없이 접근가능
    object BasicObjectClass {
        var name: String = ""
        var type: String = ""
        var num: Int = 0
        fun myinfo() {
            Log.d("yck", "name : ${name} / type : ${type} / name : ${num} /")
        }
    }


    //◈ 5. Enum Class(열거형 클래스)
    enum class BasickEnumClass {
        TIGER, ELEPHANT, LION
    }


    //◈ 5-1. Enum Class(열거형 클래스)
    enum class BasicEnumClass2(val level: Int, val type: String) {
        TIGER(2, "2str"), ELEPHANT(1, "2str"), LION(3, "3str")
    }
    //위의 코드는 BasickEnumClass2 = BasicEnumClass2 가 된거라고 생각하면 된다.
    //BasicEnumClass2(val num:Int,val type:String) == TIGER(level,type) 으로 데이터가 들어갔다고 보면된다.


    //◈ 6. inner Class
    class Outer {
        //코틀린은 class 안에 class 를 넣으면 정적 클래스가 된다.
        val bar: Int = 0

        class Nested {
            fun foo() {
                //Log.d("yck","bar: ${bar}") => 접근 불가능
            }
        }

        inner class Nested2 {
            fun foo() {
                Log.d("yck", "bar: ${bar}") //접근 가능
            }
        }
    }


    //◈ 7. 추상 클래스
    abstract class abstractClass(){
        abstract fun custom()
    }

    //◈ 7-1. 추상 클래스
    class abstractMainClass():abstractClass(){
        override fun custom() {
            print("Test")
        }
    }



}//class end





