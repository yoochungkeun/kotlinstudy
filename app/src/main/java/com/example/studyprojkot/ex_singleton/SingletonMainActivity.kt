 package com.example.studyprojkot.ex_singleton

import android.os.Bundle
import android.util.Log
import com.example.studyprojkot.BaseActivity

class SingletonMainActivity:BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var single = Singleton.getInstance(this)


        var test = single.javaClass.simpleName

        Log.d("yckcc","${test}")

    }


}//class end