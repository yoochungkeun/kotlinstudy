package com.example.studyprojkot.ex_singleton

import android.content.Context


class Singleton private constructor(){

    companion object {
        private var instance: Singleton? = null
        private lateinit var context: Context

        fun getInstance(_context: Context): Singleton{
            return instance?: synchronized(this){
                instance?: Singleton().also {
                    context = _context
                    instance = it
                }
            }
        }
    }


}