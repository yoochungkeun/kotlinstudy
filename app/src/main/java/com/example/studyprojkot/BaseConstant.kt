package com.example.studyprojkot

class BaseConstant {

    companion object{
        const val DATABINDING_WEB_IMAGE_URL = "https://search.pstatic.net/common/?src=http%3A%2F%2Fblogfiles.naver.net%2F20110415_31%2Fshin_128_1302865427818RCx9W_JPEG%2F%25B1%25CD%25BF%25A9%25BF%25EE%25C0%25CC%25B9%25CC%25C1%25F6000018.JPG&type=sc960_832"
        const val LIFECYCLE_OBSERVER_TAG = "MyObserver"
    }

}