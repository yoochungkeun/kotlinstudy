package com.example.studyprojkot.ex_viewmodel.first_way

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

//ViewModel
class MainViewModel:ViewModel() {

    //LiveData 연결
    var mVmLiveData = MutableLiveData<DataModel>()


    //입력된값 출력 전달
    fun update(value:String){

        //DataModel 에 있는 String 값에 data 셋팅하기
        val datamodel = DataModel().apply {
            inputStr = value
            outStr = value
        }

        //ViewModelActivityMain 에 셋팅된 observe 로 전달함(ViewModelActivityMain 에서 it 으로 해당 value 값을 받음)
        mVmLiveData.postValue(datamodel)
        Log.d("yck","in MainVieModel observe")
    }



}//class end