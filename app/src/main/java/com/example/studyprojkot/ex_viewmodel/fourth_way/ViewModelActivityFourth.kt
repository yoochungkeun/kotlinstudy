package com.example.studyprojkot.ex_viewmodel.fourth_way

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import com.example.studyprojkot.BaseActivity
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class ViewModelActivityFourth: BaseActivity() {

    val exampleViewModel:ExampleViewModelFourth by viewModel()

    val inject_0 by inject<InjectCountDataFourth>()
    val inject_1 by inject<InjectCountDataFourth>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

/*
        //inject 대신 viewModel() 로 주입 시켰기 때문에 아래의 코드는 필요하지 않다.
        val androidViewModelFactory = ViewModelProvider.AndroidViewModelFactory.getInstance(application)
        exampleViewModel = ViewModelProvider(this,androidViewModelFactory).get(ExampleViewModelFourth::class.java)
*/


        exampleViewModel.exampleLiveData.observe(this, Observer {
            Log.d("yck","value : ${it.value}")
        })

        exampleViewModel.prinHello()
        exampleViewModel.requestDataFourth()

        inject_0.printCount()
        inject_1.printCount()
    }


}//class end