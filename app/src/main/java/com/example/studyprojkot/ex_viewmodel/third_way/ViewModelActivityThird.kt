package com.example.studyprojkot.ex_viewmodel.third_way

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.studyprojkot.BaseActivity
import org.koin.android.ext.android.inject

//View
class ViewModelActivityThird: BaseActivity() {

    lateinit var exampleViewModel:ExampleViewModelThird

    //InjectCountData 를 사용하기 위해 의존성을 부여하는 작업임
    //! ViewModelApplication 에 module을 생성해 주어야 사용 가능함(컴파일 에러는 없으나, 정상 작동 안됨)

    //사용은 객체 생성하듯이 사용하여
    val inject_0 by inject<InjectCountData>()
    val inject_1 by inject<InjectCountData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val androidViewModelFactory = ViewModelProvider.AndroidViewModelFactory.getInstance(application)
        exampleViewModel = ViewModelProvider(this,androidViewModelFactory).get(ExampleViewModelThird::class.java)

        exampleViewModel.exampleData.observe(this, Observer {
            Log.d("yck","${it.value}")
        })

        exampleViewModel.requestDate()
        exampleViewModel.printHello()

        inject_0.printCount()
        inject_1.printCount()
    }

}//class end