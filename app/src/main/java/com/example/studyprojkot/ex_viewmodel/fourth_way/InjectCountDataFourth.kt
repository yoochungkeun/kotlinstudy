package com.example.studyprojkot.ex_viewmodel.fourth_way

import android.util.Log

class InjectCountDataFourth {

    companion object{
        var injectCount = 0
    }

    init {
        injectCount++
    }

    fun printCount(){
        Log.d("yck","injectCountData injectCount ${injectCount}")
    }

}//class end