package com.example.studyprojkot.ex_viewmodel.fourth_way

import android.content.Context

class PackageRepositoryFourth(context: Context) {
    val packageName:String
    init {
        packageName = context.packageName
    }
}//class end