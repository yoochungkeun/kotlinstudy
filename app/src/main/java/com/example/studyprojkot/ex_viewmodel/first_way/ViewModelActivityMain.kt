package com.example.studyprojkot.ex_viewmodel.first_way

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import com.example.studyprojkot.R
import com.example.studyprojkot.BaseActivity
import kotlinx.android.synthetic.main.activity_viewmodel_main.*

//View
class ViewModelActivityMain: BaseActivity() {

    //ViewModel 객체생성
    val mMainViewModel =  MainViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_viewmodel_main)

        setObserve()

        //버튼 눌렀을때, 입력된 값 출력하기
        btn_vm.setOnClickListener {
            updateText()
            Log.d("yck","OnClickListener")
        }
    }

    //ViewModel 에게 입력된값 출력 요청
    fun updateText(){
        mMainViewModel.update(edt_vm.text.toString())
    }

    //ViewModel LiveData 등록시키기
    fun setObserve(){
        mMainViewModel.mVmLiveData.observe(this, Observer {
            var getValue = it.inputStr
            var getValue2 = it.outStr

            Log.d("yck","getValue : ${getValue}")
            Log.d("yck","getValue2 : ${getValue2}")

        })
    }


}//class end