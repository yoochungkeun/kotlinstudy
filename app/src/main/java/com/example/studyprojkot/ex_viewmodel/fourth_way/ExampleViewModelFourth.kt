package com.example.studyprojkot.ex_viewmodel.fourth_way

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


//ViewModel
class ExampleViewModelFourth(val printServiceFourth: PrintServiceFourth):ViewModel() {

    private val _exampleLiveData:MutableLiveData<ExampleModelDataFourth> = MutableLiveData()

    val exampleLiveData:LiveData<ExampleModelDataFourth>
        get() = _exampleLiveData

    //Data 값 변경 해 줄 함수
    fun requestDataFourth(){
        //Data 에서 값을 변경하기
        val exampleData = ExampleModelDataFourth().apply {
            value = 23231
        }
        _exampleLiveData.value = exampleData
    }

    //또 다른 실행 함수
    fun prinHello(){
        printServiceFourth.printHello()
    }

    override fun onCleared() {
        super.onCleared()
    }

}//class end