package com.example.studyprojkot.ex_viewmodel.second_way

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.studyprojkot.BaseActivity

/*View*/
class ViewModelActivitySecond: BaseActivity() {

    //ViewModel 연결할 프로퍼티
    lateinit var exampleViewModel:ExampleViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //ViewModel 연결 준비
        val androidViewModelFactory = ViewModelProvider.AndroidViewModelFactory.getInstance(application)

        //ViewModel 생성
        //!!!!!!!!!! view 를 담당하는 클래스는 반드시 AppCompatActivity 을 상속받아야 한다, Activity 는 안된다.
        exampleViewModel = ViewModelProvider(this,androidViewModelFactory).get(ExampleViewModel::class.java)

        //ViewModel 에 LiveData 옵져버 등록
        exampleViewModel._exampleLiveData.observe(this, Observer {
            Log.d("yck","${it.value}")
        })

        //ViewModel 에 있는 작업 할 함수 실행
        exampleViewModel.changeValue()
    }

}//class end