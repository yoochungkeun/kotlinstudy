package com.example.studyprojkot.ex_viewmodel.fourth_way

import android.app.Application
import com.example.studyprojkot.ex_viewmodel.third_way.InjectCountData
import org.koin.android.ext.android.startKoin
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

class VIewMomdelApplicationFourth:Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(
            androidContext = applicationContext,
            modules = listOf(myViewModel, myModule)

        )
    }

}//class end

val myViewModel= module {
    viewModel {
        ExampleViewModelFourth(get())
    }
}

val myModule = module {

    single {
        PackageRepositoryFourth(androidContext())
    }

    single {
        PrintServiceFourth(get())
    }

    single {
        InjectCountDataFourth()
    }
}