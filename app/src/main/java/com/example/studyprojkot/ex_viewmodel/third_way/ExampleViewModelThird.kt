package com.example.studyprojkot.ex_viewmodel.third_way

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

//ViewModel
class ExampleViewModelThird:ViewModel(),KoinComponent{
//KoinComponent => 해당 클래스에서 다른 클래스의 의존성을 주입하기 위해(by inject) 받는 interface

    //Data 담을 liveData 프로퍼티
    private val _exampleLiveDatae:MutableLiveData<ExampleModelDataThird> = MutableLiveData()

    val printService:PrintService by inject()

    val exampleData:LiveData<ExampleModelDataThird>
        get() = _exampleLiveDatae

    override fun onCleared() {
        super.onCleared()
        Log.d("yck",">>> onCleared()")
    }

    //데이터 변화 함수
    fun requestDate(){
        val exampleData = ExampleModelDataThird().apply {
            value = 12333
        }
        _exampleLiveDatae.value = exampleData
    }

    //class PrintServece 의 함수를 실행한다.
    //! ViewModelApplication > module 등록이 되어있지 않으면 사용 할 수 없다.
    fun printHello(){
        printService.printHello()
    }


}//class end