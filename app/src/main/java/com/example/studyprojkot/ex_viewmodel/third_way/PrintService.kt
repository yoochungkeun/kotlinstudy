package com.example.studyprojkot.ex_viewmodel.third_way

import android.util.Log

class PrintService(val packageRepository: PackageRepository){

    fun printHello(){
        Log.d("yck","PrintService Hello ${packageRepository.packageName}")
    }

}