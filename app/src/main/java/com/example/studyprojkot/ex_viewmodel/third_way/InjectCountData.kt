package com.example.studyprojkot.ex_viewmodel.third_way

import android.util.Log

class InjectCountData {

    companion object{
        var injectCount = 0
    }

    init {
        injectCount++
    }

    fun printCount(){
        Log.d("yck","injectCountData injectCount ${injectCount}")
    }

}//class end