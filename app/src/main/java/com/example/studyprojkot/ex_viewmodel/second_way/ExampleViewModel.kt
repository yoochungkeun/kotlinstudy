package com.example.studyprojkot.ex_viewmodel.second_way

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


/*ViewModel*/
class ExampleViewModel:ViewModel() {

    val _exampleLiveData:MutableLiveData<ExampleModelData> = MutableLiveData()

    fun changeValue(){
        val exampleData = ExampleModelData().apply {
            value = 1000
        }
        _exampleLiveData.value = exampleData
    }

    override fun onCleared() {
        super.onCleared()
        Log.d("yck",">>> onCleared()")
    }

}//class end