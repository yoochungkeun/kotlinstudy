package com.example.studyprojkot.ex_viewmodel.fourth_way

import android.util.Log

class PrintServiceFourth(val packageRepository: PackageRepositoryFourth){

    fun printHello(){
        Log.d("yck","PrintService Hello ${packageRepository.packageName}")
    }

}