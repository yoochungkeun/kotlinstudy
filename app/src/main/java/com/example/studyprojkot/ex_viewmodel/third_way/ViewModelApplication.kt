package com.example.studyprojkot.ex_viewmodel.third_way

import android.app.Application
import org.koin.android.ext.android.startKoin
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module

class ViewModelApplication:Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin(
            androidContext = applicationContext,
            modules = listOf(myModule)
        )
    }
}//class end



//DSL 키워드(koin 사용시 알아야할 키워드)
/*
- module : Koin모듈을 정의할때 사용
- factory : inject하는 시점에 해당 객체를 생성
- single : 앱이 살아있는 동안 전역적으로 사용가능한 객체를 생성
- bind : 생성할 객체를 다른 타입으로 바인딩 하고 싶을때 사용
- get : 주입할 각 컴포넌트끼리의 의존성을 해결하기 위해 사용
*/
val myModule = module {
    //싱글톤 구조
    single {
        //context를 파라미터로 생성하는 PrintService 클래스
        PackageRepository(androidContext())
    }

    single {
        //PackageRepository 객체로 생성되는 PrintService 클래스
        //get()생성에 필요한 파라미터를 찾아서 주입한다.
        //PrintService (val packgeRepository:PackageRepository)
        PrintService(get())
    }

    //주입 할 때 마다 injectcountData 객체 생성
    factory {
        InjectCountData()
    }
}