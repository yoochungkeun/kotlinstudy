package com.example.studyprojkot.ex_property_getset

import android.os.Bundle
import android.util.Log
import com.example.studyprojkot.BaseActivity


//Property
//Java 로 치면 변수이다. 변수인데 클래스처럼 변수 자체적으로 get/set 을 가질 수 있는것이 Java 변수와는 다른 점이다.
//var 과 val 로 선언 할 수 있다. (var => 변수 / val => 상수)
class PropertyActvitiyMain : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //[getter / setter]

        //in Kotlin
        var dog = Dog()
        Log.d("yck","dog.name : "+dog.name)
        dog.name = "Doldory"
        dog.old = 20

        //in JAVA
/*
        Dog dog = new Dog();
        Log.d("yck","dog.getName() : "+dog.getName());
        dog.setName("Doldory");
        dog.setOld(20);
*/
    }

//in Kotlin
    class Dog{
        var name ="Backu"
            get() {
                return field
            }
            set(value) {
                Log.d("yck","name value : "+value)
            }
        var old = 14
            get() {
                return field
            }
            set(value) {
                Log.d("yck","old value : "+value)
            }
    }


//in JAVA
/*
    // 코틀린으로 치면, Dog 라는 클래스는 name 과 old 라는 property 를 가지고 있는 클래스이다
    class Dog{
        private String name ="Backu";
        private int old =14;

        //name에 실제 값(field)에 접근 할 수 있다.
        public String getName() {
            return name;
        }
        //name에 실제 값(field)에 접근 할 수 있다.
        public void setName(String name) {
            this.name = name;
        }
        //old에 실제 값(field)에 접근 할 수 있다.
        public int getOld() {
            return old;
        }
        //old에 실제 값(field)에 접근 할 수 있다.
        public void setOld(int old) {
            this.old = old;
        }
    }
    //-> field(필드) : 정의된 내용의 실제값 이다.
    //-> property(속성) : gettter 와 setter 처럼, 메소드를 가지는 field 이다.
*/




}//class end