package com.example.studyprojkot.ex_databinding

import android.app.Activity
import androidx.databinding.DataBindingUtil
import com.example.studyprojkot.R
import com.example.studyprojkot.databinding.ActivityDatabindingMainBinding


class DataBindingUI(activity: Activity) {

    private var activity = activity
    private lateinit var binding: ActivityDatabindingMainBinding

    fun setTitleView(){
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_databinding_main)
        binding.bindModel = DataBindingModel("text-1","text-2","버튼")


//        binding.btn.setOnClickListener {
//            binding.tv.text = "welcome data binding"
//        }
    }


}//class end