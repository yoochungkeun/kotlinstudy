package com.example.studyprojkot.ex_databinding

import android.os.Bundle
import com.example.studyprojkot.BaseActivity


/*
단순히 View를 설정해주는 클래스에서 binding 시켜,
해당 클래스에서 선언하여 바로 사용하는 방법
*/
class DataBindingMainActivity: BaseActivity() {

    lateinit var databindingUi: DataBindingUI

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //DataBindingUI 로 넘겨 해당 클래스에서 처리하도록 만듦
        databindingUi =
            DataBindingUI(this)
        databindingUi.setTitleView()
    }


}//class end