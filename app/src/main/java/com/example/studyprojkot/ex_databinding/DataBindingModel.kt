package com.example.studyprojkot.ex_databinding

data class DataBindingModel (
    var text1:String,
    var text2:String,
    var btn:String
)