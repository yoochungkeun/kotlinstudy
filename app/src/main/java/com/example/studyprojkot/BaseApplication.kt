package com.example.studyprojkot

import android.app.Application
import com.example.studyprojkot.ex_koin.first_way.SchoolService
import com.example.studyprojkot.ex_koin.first_way.StudentController
import org.koin.android.ext.android.startKoin
import org.koin.dsl.module.module
import timber.log.Timber

//AndroidManifest.xml > Application 적용해서 사용
class BaseApplication:Application(){

    //DSL 키워드
    /*
    - module : Koin모듈을 정의할때 사용
    - factory : inject하는 시점에 해당 객체를 생성
    - single : 앱이 살아있는 동안 전역적으로 사용가능한 객체를 생성
    - bind : 생성할 객체를 다른 타입으로 바인딩 하고 싶을때 사용
    - get : 주입할 각 컴포넌트끼리의 의존성을 해결하기 위해 사용
    - named :  enum 이나 string으로 한정자를 정의해줌
    - getProperty : 필요한 프로퍼티를 가져옴
    */

    val appModule = module {
        single{ SchoolService() }
        single{ StudentController(get()) }
    }

    override fun onCreate() {
        super.onCreate()
        startKoin(
            androidContext = applicationContext,
            modules = listOf(appModule)
        )

        //Timber 초기화
        Timber.plant(Timber.DebugTree())

    }

}//class end