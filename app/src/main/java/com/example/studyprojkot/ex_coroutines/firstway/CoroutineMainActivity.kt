package com.example.studyprojkot.ex_coroutines.firstway

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.annotation.UiThread
import com.example.studyprojkot.BaseActivity
import kotlinx.coroutines.*

class CoroutineMainActivity :BaseActivity(){

    //Coroutine :
    //스레드와 기능적으로 같지만, 스레드에 비교하면 좀 더 가볍고 유연하며 병렬프로그래밍을 위한 기술
    //하나의스레드 내에서 여러개의 코루틴이 실행되는 개념이다.
    //GlobalScorpe.launch로 정의되며, {...} 으로 묶은 코드가 비동기 적으로 실행된다.

    //ex)예제 를 실행하면, mainA가 실행하면서 진행이 되고, 코루틴 외에 있는 함수 mainC > mainD 순으로 진행된다.
    //그리고mainA가 끝나면 mainB가 실행되고 mainB가 완료되는 프로세스 만들기

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//코루틴 사용 X -> main in B start > main in B end > main in C > main in D
//main in B start 가 끝날때까지 스레드는 기다리게 된다.
        mainB()
        mainC()
        mainD()

//코루틴
/*
//<해당 함수가 끝날때까지 블록 안에 프로세스를 잡아주는 역할 을 한다.>
- GlobalScope.run : 블록 내부에서 부모의 activity 를 받을 수 없다.
- GlobalScope.runCatching : 블록 내부에서 부모의 activity 를 받을 수 없다.
- runBlocking : 블록 내부에서 부모의 activity 를 받을 수 없다.
- runCatching : 블록 내부에서 부모의 activity 를 가져갈 수 있다.
*/

//실행순서 => mainB start > mainB end > mainC > mainD
        GlobalScope.run {
            mainB()
        }
        mainC()
        mainD()


//코루틴
/*
//<블록 밖에 함수가 종료 된후에, 블록 안에 프로세스를 실행 해 준다.>
- GlobalScope.launch : 블록 내부에서 부모의 activity 를 받을 수 없다.
- GlobalScope.async : 블록 내부에서 부모의 activity 를 받을 수 없다.
*/

//실행순서 => mainC > mainD > mainB start > mainB end
/*
        GlobalScope.launch {
            mainB()
        }
        mainC()
        mainD()
*/
    }

    fun mainB(){
        Log.d("yck","main in B start")
        runOnUiThread {
            Thread.sleep(3000)
            Log.d("yck","main in B end")
        }
    }

    fun mainC(){
        Log.d("yck","main in C")
    }

    fun mainD(){
        Log.d("yck","main in D")
    }


}//class end