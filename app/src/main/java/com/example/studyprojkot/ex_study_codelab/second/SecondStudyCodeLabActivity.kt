package com.example.studyprojkot.ex_study_codelab.second

import android.app.Activity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.example.studyprojkot.R
import com.example.studyprojkot.databinding.ActivitySecondStudyCodelabBinding

class SecondStudyCodeLabActivity : Activity(){



    //view xml Binding
    private lateinit var mBinding:ActivitySecondStudyCodelabBinding

    //init Data Class
    private val mMyName = MyNameData()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //init view xml Binding
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_second_study_codelab)

        //view xml Binding link to Data class
        mBinding.myName = mMyName

        //방법.1 set Data to view xml Binding views
        /*
                mBinding.apply {
                    myName?.name = "네임"
                    myName?.nickname = "별명니니니"
                    myName?.fullname = "네임별명니니니"
                }
        */

        //방법.2 순수class 로 던져서 view 처리
        var secondClass = SecondSubClass()
        secondClass.testBinding(this,mBinding)
    }





}//class end