package com.example.studyprojkot.ex_study_codelab.second

data class MyNameData(
    var name:String="",
    var nickname:String="",
    var fullname:String=""
)
