package com.example.studyprojkot.ex_study_codelab.second

import android.app.Activity
import com.example.studyprojkot.R
import com.example.studyprojkot.databinding.ActivitySecondStudyCodelabBinding
import timber.log.Timber

class SecondSubClass {

    //Data binding 을 이용해  일반적인 클래스에서 param 으로 전달받아서, 구현 할 수 있다.
    fun testBinding(activity: Activity, binding:Any){
        var thisBinding = binding as ActivitySecondStudyCodelabBinding
        thisBinding.apply {
            myName?.name = "이르므2"
            myName?.nickname = "별며으2"
            myName?.fullname = "풀네이므2"
            invalidateAll()
        }
        //SecondStudyCodeLabActvitiy 에서 binding Data 로 xml view 제어하기
        thisBinding.tvName.setBackgroundColor(activity.resources.getColor(R.color.color_4f6990))

        //xml dataBinding 은 MyNameData class 로 바로 연결되어져셔 출력되는것이기때문에 text 를 가져오면, 아무 값도 보이지 않는다.
        Timber.w("thisBinding.tvName.text : ${thisBinding.tvName.text}")
        //DataClass 의 값을 가져와야 보인다.
        Timber.i("thisBinding.myName?.fullname : ${thisBinding.myName?.fullname}")
    }


}//class end
