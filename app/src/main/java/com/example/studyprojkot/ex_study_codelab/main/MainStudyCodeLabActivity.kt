package com.example.studyprojkot.ex_study_codelab.main

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.example.studyprojkot.R
import kotlinx.android.synthetic.main.activity_study_codelab.*

class MainStudyCodeLabActivity :Activity(){


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_study_codelab)

        val redButton = findViewById<Button>(R.id.btn_1)
        val greenButton = findViewById<Button>(R.id.btn_2)
        val yellowButton = findViewById<Button>(R.id.btn_3)

        val viewlist = listOf(redButton,greenButton,yellowButton)

        for(items in viewlist){
            items.setOnClickListener{
                makeColored(it)
            }
        }


    }






    private fun makeColored(view:View){
        when(view.id){
            R.id.btn_1 -> btn_1.setBackgroundColor(resources.getColor(R.color.color_e1584b))
            R.id.btn_2 -> btn_2.setBackgroundColor(resources.getColor(R.color.color_71c890))
            R.id.btn_3 -> btn_3.setBackgroundColor(resources.getColor(R.color.color_f8e81c))
            else -> view.setBackgroundColor(resources.getColor(R.color.black))
        }



    }



}//class end