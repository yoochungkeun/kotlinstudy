package com.example.studyprojkot.ex_observable.first_way

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.example.studyprojkot.BR

class ObservableData:BaseObservable() {

    @get:Bindable
    var text:String=""

    set(value) {
        field = value
        notifyPropertyChanged(BR.site)
    }

/*
//- general get/set
    class getsetClass(){
         var name:String = ""
         set(value) {
             field = value
         }
    }
*/


}//class end