package com.example.studyprojkot.ex_observable.first_way

import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import com.example.studyprojkot.BaseActivity
import com.example.studyprojkot.R
import com.example.studyprojkot.databinding.ActivityObservableMainBinding


class ObservavleActivityMain: BaseActivity() {

/*
    val binding by lazy {
        DataBindingUtil.setContentView<ActivityObservableMainBinding>(this@ObservavleActivityMain,
            R.layout.activity_observable_main
        )
    }
*/

    lateinit var binding: ActivityObservableMainBinding


    var item : ObservableData =  ObservableData()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@ObservavleActivityMain,R.layout.activity_observable_main)

        item.text ="Naver"//테스트 기본값 셋팅
        updteData()

        binding.btn.setOnClickListener {
            item.text = "Google"//클릭시 observableData 값 변경 해주고, 해당 item 을 binding xml 에 업데이트 해준다.
            updteData()
        }
    }


    fun updteData(){
        binding.site = item
    }


}//class end