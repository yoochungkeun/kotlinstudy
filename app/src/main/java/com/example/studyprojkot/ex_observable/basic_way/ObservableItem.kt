package com.example.studyprojkot.ex_observable.basic_way

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable

class ObservableItem : BaseObservable() {

    var name: String = ""
        @Bindable
        get() = field
        set(value) {
            field = value
        }

    var nickName: String = ""
        @Bindable
        get() = field
        set(value) {
            field = value
        }


}//class end