/*
package com.example.studyprojkot.ex_observable.basic_way

import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import com.example.studyprojkot.R
import com.example.studyprojkot.databinding.ActivityObservableBasicBinding
import com.example.studyprojkot.BaseActivity

class BasicObservableActivityMain: BaseActivity(){

    //DataBinding 을 통해 layout.xml은 연결시킴
    lateinit var databinding:ActivityObservableBasicBinding

    //lyaout.xml 과 Item class 인 class ObservableItem을 인스턴스화 시킴
    val observableItem:ObservableItem by lazy {
        ObservableItem()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        databinding = DataBindingUtil.setContentView(this@BasicObservableActivityMain,R.layout.activity_observable_basic)
        databinding.item = observableItem

        observableItem.name="우끼끼"

        databinding.btn.setOnClickListener {
            onAction()
        }

    }


    fun onAction(){
        observableItem.name="우끼끼"
        observableItem.nickName = "갸끼끼"
        //값 변경 안됨!!!!
    }


}//class end
*/
