package com.example.studyprojkot.ex_scoping_func

import android.app.Activity
import android.os.Bundle
import android.util.Log
import com.example.studyprojkot.BaseActivity
import kotlin.math.log

class ScopingFuncActivityMain : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //also_func()
        let_func()

    }

    fun apply_func() {
/*
        //apply 미사용
        val adam = ScopingFuncSub("Adam")
        adam.age = 20
        adam.city = "London"
*/
        //apply 사용
        val adam = ScopingFuncSub("Adam").apply {
            age = 10
            city = "London"
        }
    }

    fun let_func() {
//        val str: String? = null
//
//        val length = str?.let {
//            it.length
//        }
//        Log.d("scoping_","let() length : ${length}")
//
//        val length2 = str?.length
//        Log.d("scoping_","let() length2 : ${length2}")
    }

    fun run_func() {

    }

    fun also_func() {
        //내부에 있는 값을 it 으로 통째로 받을수 있다. 추가로 값도 변경 가능하다.

//        val numbers = mutableListOf("one", "two", "three")
//        numbers.also {
//            Log.d("scoping_", "it : ${it}")
//            //[one, two, three]
//            val gettest = it.get(0)
//            Log.d("scoping_", "it : ${gettest}")
//        }.add("0")
//
//        numbers.also {
//            Log.d("scoping_", "it : ${it}")
//            //[one, two, three, 0]
//        }

    }

}//class end