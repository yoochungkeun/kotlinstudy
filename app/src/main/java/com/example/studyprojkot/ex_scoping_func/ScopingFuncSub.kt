package com.example.studyprojkot.ex_scoping_func

class ScopingFuncSub(name: String) {

    var mName = name

    var age = 0
        get() {//getter
            return field
        }
        set(value) {//setter
            this.age = value
        }

    var city = ""
        get() {//getter
            return field
        }
        set(value) {//setter
            this.city = value
        }

}