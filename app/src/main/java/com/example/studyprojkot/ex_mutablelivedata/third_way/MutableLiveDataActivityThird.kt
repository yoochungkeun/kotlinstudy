package com.example.studyprojkot.ex_mutablelivedata.third_way

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.studyprojkot.BaseActivity

class MutableLiveDataActivityThird: BaseActivity() {

    //model class liveDta 연결
    private val _exampleLiveData:MutableLiveData<ExampleData> = MutableLiveData()
//?
//    val exampleData:LiveData<ExampleData>
//        get() = _exampleLiveData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        _exampleLiveData.observe(this, Observer {
            Log.d("yck","${it.value}")
        })

        val exampleData = ExampleData().apply {
            value = 10000
        }

        _exampleLiveData.postValue(exampleData)
    }

}
