package com.example.studyprojkot.ex_mutablelivedata.first_way

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.studyprojkot.R
import com.example.studyprojkot.BaseActivity
import kotlinx.android.synthetic.main.activity_livedata_main.*

class MutableLiveDataActivityMain: BaseActivity() {

    private var liveText:MutableLiveData<String> = MutableLiveData()
    private var count = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_livedata_main)

        //[1]
        //LiveDta의 observe 를 연결시킴
        liveText.observe(this, Observer {
            text_test.text = it
        })

        //[2]
        //버튼을 클릭하면 Hellow World! 숫자증가 출력함
        btn_change.setOnClickListener {
            liveText.value ="Hello World! ${++count}"
            //liveText의 value를 변경하면 위의 [1]의 Observer Callback이 호출되면서,
            //이곳의 value 가 [1]의 it 으로 값이 전달된다.
        }
    }

}//class end