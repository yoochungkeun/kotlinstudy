package com.example.studyprojkot.ex_mutablelivedata.second_way

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.studyprojkot.R
import com.example.studyprojkot.BaseActivity
import kotlinx.android.synthetic.main.activity_livedata_main.*

class MutableLiveDataActivitySecond: BaseActivity() {
    private var count = 0
/*
    //[1]
    // apply block 사용한 방법
    private var liveText:MutableLiveData<String> = MutableLiveData<String>().apply {
        value = "Hello world!${++count}"
    }
*/

    //[2]
    private var liveText:MutableLiveData<String> = MutableLiveData<String>().set("Hello world!${++count}")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_livedata_second)

/*
        //[1-1]
        //[1]방법 사용시 연속으로 숫자를 증가시킬수는 없다.
        btn_change.setOnClickListener {
            liveText.observe(this, Observer {
                text_test.text = it
            })
        }
*/

        //[2]
        //[2-1]방법 사용시 연속으로 숫자를 증가시킬수는 없다.
        btn_change.setOnClickListener {
            liveText.observe(this, Observer {
                text_test.text = it
            })
        }

    }

    //[2]
    private fun MutableLiveData<String>.set(value:String):MutableLiveData<String>{
        this.value = value
        return this
    }


}//class end