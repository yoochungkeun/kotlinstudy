package com.example.studyprojkot.ex_var_getset

import android.app.Activity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity

class VarActivityMain :AppCompatActivity() {

    //in Kotlin
    val numVal:Int = 0 //java 의 final 이라기보다는 더 정확하게 읽기 전용 이다.(final이랑 다른 이유는 ex_const_final 에 기록함)
    val strVal:String = ""
    var numvar:Int = 0//더 정확하게는 읽기/쓰기 전용 이다.
    var strvar:String = ""

    //in JAVA
    /*
    final int numVal = 0;
    final String strVal = "";
    int numvar = 0;
    String strvar = "";
    */

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var actionBar = supportActionBar
        actionBar!!.title = javaClass.simpleName

        //in Kotlin
        var getset_class = getsetClass()
        Log.d("yck","getset_class.name : "+getset_class.name)

        //in JAVA
        //getsetClass getset_class = new getsetClass();
        //Log.d("tt","getset_class.getName() : "+getset_class.getName());
    }

//in Kotlin
    class getsetClass(){
        var name:String = "겟셋들"
            get() = field  //Back
            set(value) {
                field = value
            }
    }

//in JAVA
/*
    public class getsetClass{
        String name = "겟셋들";
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
    }
*/


//[Getter,Setter]
/*
    var testString:String = "스트링이다"
        get() = field

        private set(value) {
            field = value
        }

    - testString => Property Name(이름)
    - :String => Property Type(타입)
    - "스트링이다" => Property initializer(초기화)
    - get() => gettter
    - set() {  } => setter
    - private => Accessor(접근자)
*/



}//class end