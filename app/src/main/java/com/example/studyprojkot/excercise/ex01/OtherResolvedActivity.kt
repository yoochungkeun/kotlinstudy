package com.example.studyprojkot.excercise.ex01

import android.app.Activity
import android.os.Bundle
import android.util.Log

class OtherResolvedActivity : Activity() {

    val ex1 = "...!@BaT#*..y.abcdefghijklm"
    //=> "bat.y.abcdefghi"

    val ex2 = "z-+.^."
    //=> "z--"

    val ex3 = "=.="
    //=> "aaa"

    val ex4 = "123_.def"
    //=> "123_.def"

    val ex5 = "abcdefghijklmn.p"
    //=> "abcdefghijklmn"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var testValue = ex1
        Log.e("final_yck0936","ResolvedActivity/testValue : ${testValue}")
        Log.e("final_yck0936","ResolvedActivity/testValue : ${solution(testValue)}")

        var dddd = "sksjiejsklsdnflnsla"

        var tttt = dddd.substring(0 until 4)

        Log.e("final_yck0936","ResolvedActivity/tttt.first : ${tttt.first()}")
    }


    fun solution(newId: String) = newId.toLowerCase()
        .filter {
            it.isLowerCase() || it.isDigit() || it == '-' || it == '_' || it == '.'
        }.replace("[.]*[.]".toRegex(), ".")
        .removePrefix(".").removeSuffix(".")
        .let {
            if (it.isEmpty()) "a" else it
        }
        .let {
            if (it.length > 15) it.substring(0 until 15) else it
        }.removeSuffix(".")
        .let {
            if (it.length <= 2)
                StringBuilder(it).run {
                    while (length < 3) append(it.last())
                    toString()
                }
            else it
        }
    //.removePrefix(A)    => 문자열 맨 앞에 A와 동일 하면 제거
    //.removeSuffix(A)    => 문자열 맨 뒤에 A와 동일 하면 제거
    //.substring(0 until 15)    => 문자열 0 ~ 15 까지 잘라내기
    //.last()    => 문자열 맨 뒤에 Char 로 1개 추출
    //.first()    => 문자열 맨 앞에 Char 로 1개 추출
}