package com.example.studyprojkot.excercise.ex03

import android.os.Bundle
import android.util.Log
import com.example.studyprojkot.BaseActivity
import kotlin.math.roundToInt

class MyReolvedMainActivity03:BaseActivity() {

    val LOGNAME = "yck_ex_03"

/*
    N	stages	                    result
    5	[2, 1, 2, 6, 2, 4, 3, 3]	[3,4,2,1,5]
    4	[4,4,4,4,4]	                [4,1,2,3]
*/

    var ex01_stages = intArrayOf(2,1,2,6,2,4,3,3)
    var ex02_stages = intArrayOf(4,4,4,4,4)

    //N번 돌려야함 1~N번 까지 (1,2,3,4,5스테이지 이런식)

    //1. 1스테이지 일때 배열 체크 1포함 1보다 큰 숫자 갯수 추출
    //2. 1스테이지 일때 배열 체크 같은숫자 갯수 추출
    //3. 2번을 1번으로 나눈값 추출 (비교를 위해 배열에 해당 스테이지와 함께 담아둠)
    //4. 1번부터 3번까지 과정은 N번 반복
    //5. 반복 완료후 해당 스테이지와 함께 담아둔 3번의 값을 비교
    //6. 비교후 내림차순(큰 숫자 맨앞으로 답변 배열에 담음)
    //6-1. 배열에 담을때 비교한 값이 동일할때 , 스테이지가 작은 값을 먼저 담음
    //7. 6번에서 담은 배열 리턴 - 끝 -

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val intArr = solution(4,ex02_stages)
        intArr.forEach {
            Log.d("result_yck","result  :  ${it}")
        }

    }

    fun solution(N: Int, stages: IntArray): IntArray {
        var answer = IntArray(N,{0})

        var checkHash = mutableMapOf<Int,Float>()
        var challengerNum = 0//도전자 수
        var challengingUserNum = 0 //도전중인 수

        for(i in 1..N){

            for(j in  0 until stages.size){
                if(stages[j]>=i){//해당 스테이지 성공자
                    challengerNum++
                }
                if(stages[j]==i){//해당 스테이지 실패자
                    challengingUserNum++
                }
            }

            if(challengingUserNum!=0 && challengerNum!=0){
                var failedPecent:Float = (challengingUserNum.toFloat() / challengerNum.toFloat())

                //val failedPercentResult= (failedPecent*10).roundToInt() / 10f//소숫점 1짜리 반올림
                val failedPercentResult= (failedPecent*10) / 10f//소숫점 1짜리 반올림

                checkHash.put(i,failedPercentResult)
            }else{
                checkHash.put(i,0.0f)//실패자 없으면, 실패율 0 저장
            }
            challengerNum = 0
            challengingUserNum = 0
        }

        val sortingCheckHash_descendingItrt = checkHash.toList().sortedWith(compareByDescending { it.second }).toMap().iterator()
        var num = 0
        while (sortingCheckHash_descendingItrt.hasNext()){
            val print = sortingCheckHash_descendingItrt.next()
            var printKey = print.key
            answer.set(num,printKey)
            num++
        }

        return answer
    }







}//class end