package com.example.studyprojkot.excercise.ex02

import android.app.Activity
import android.os.Bundle
import android.util.Log

class MyResolvedMainActivity02 :Activity(){

    val ex1_numbers_hand = listOf(intArrayOf(1, 3, 4, 5, 8, 2, 1, 4, 5, 9, 5),"right")
    //=> "L  R  L  L  L  R  L  L  R  R  L"
    val ex2_numbers_hand = listOf(intArrayOf(7, 0, 8, 2, 8, 3, 1, 5, 7, 6, 2),"left")
    //=> "L  R  L  L  R  R  L  L  L  R  R"
    val ex3_numbers_hand = listOf(intArrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 0),"right")
    //=> "L  L  R  L  L  R  L  L  R  L"
    val ex4_numbers_hand = listOf(intArrayOf(4, 8, 2, 9, 4, 6, 1, 0, 4, 8, 3, 5, 6, 6, 8, 6, 5, 2, 8, 8, 0, 0, 2, 4, 4),"left")
    //=> "L  L  L  R  L  R  L  R  L  R  R  L  R  R  L  R  L  L  L  L  L  L  R  L  L "
    val ex5_numbers_hand = listOf(intArrayOf(4, 8, 2, 9, 4, 6, 1, 0, 4, 8, 3, 5, 6, 6, 8, 6, 5, 2, 8, 8, 0, 0, 2, 4, 4),"right")
    //=> "L  L  L  R  L  R  L  R  L  R  R  L  R  R  L  R  L  L  L  L  L  L  R  L  L "
    val ex7_numbers_hand = listOf(intArrayOf(6, 7, 8, 0, 2, 3, 1, 2, 3, 5, 2, 6, 5, 7, 6, 1, 1, 0, 8, 3, 4, 5, 2, 5, 7, 3, 2, 1, 8 ,0, 8, 0, 0, 0, 0, 4, 3 ,2, 5, 0),"right")
    //=> "R  L  L  L  R  R  L  R  R  R  R  R  R  L  R  L  L  R  R  R  L  L  R  L  L  R  R  L  R  R  R  R  R  R  R  L  R  R  R  R"
    val ex8_numbers_hand = listOf(intArrayOf(1, 2, 2, 4, 4, 8, 8, 8, 0, 0, 0),"right")
    //=> "L  L  L  L  L  R  R  R  R  R  R "

    // 1|2|3
    // 4|5|6
    // 7|8|9
    // *|0|#

    val pad_numbers = listOf(
                             listOf(listOf(0),listOf(8),listOf(5,7,9),listOf(2,4,6),listOf(1,3)),//padNum 0
                             listOf(listOf(1),listOf(2,4),listOf(3,5,7),listOf(6,8),listOf(9,0)),//padNum 1
                             listOf(listOf(2),listOf(1,3,5),listOf(4,6,8),listOf(7,9,0)),//padNum 2
                             listOf(listOf(3),listOf(2,6),listOf(1,5,9),listOf(4,8),listOf(7,0)),//padNum 3
                             listOf(listOf(4),listOf(1,5,7),listOf(2,6,8),listOf(3,9,0)),//padNum 4
                             listOf(listOf(5),listOf(2,4,6,8),listOf(1,3,7,9,0)),//padNum 5
                             listOf(listOf(6),listOf(3,5,9),listOf(2,4,8),listOf(1,7,0)),//padNum 6
                             listOf(listOf(7),listOf(4,8),listOf(1,5,9,0),listOf(2,6),listOf(3)),//padNum 7
                             listOf(listOf(8),listOf(5,7,9,0),listOf(2,4,6),listOf(1,3)),//padNum 8
                             listOf(listOf(9),listOf(6,8),listOf(3,5,7,0),listOf(2,4),listOf(1)),//padNum 9
                             listOf(listOf(10),listOf(7,0),listOf(4,8),listOf(1,5,9),listOf(2,6),listOf(3)),//pad *
                             listOf(listOf(11),listOf(0,9),listOf(6,8),listOf(3,5,7),listOf(2,4),listOf(1))//pad #
                             )
    var leftHandTouched:Int = 10//왼손 시작 *
    var rightHandTouched:Int = 11//오른손 시작 #

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var testValue =  ex8_numbers_hand
        var result = solution(testValue.get(0) as IntArray,testValue.get(1).toString())
        Log.e("final_yck","MyTestMainActivity02/result : ${result}")
    }

    fun solution(numbers: IntArray, hand: String): String {
        var handInSolution = "L  "
        var result = ""
        if(hand.equals("right")){
            handInSolution = "R  "
        }
        for(i in 0 until numbers.size){
            if(numbers.get(i)==1||numbers.get(i)==4||numbers.get(i)==7){
                result += "L  "
                leftHandTouched = numbers.get(i)
            }else if(numbers.get(i)==3||numbers.get(i)==6||numbers.get(i)==9){
                result += "R  "
                rightHandTouched = numbers.get(i)
            }else{
                var leftHandPoint =  getPoint(leftHandTouched,numbers.get(i))
                var rightHandPoint =  getPoint(rightHandTouched,numbers.get(i))

                if(leftHandPoint<rightHandPoint){//left가 더 가까울때
                    result += "L  "
                    leftHandTouched = numbers.get(i)
                }else if(leftHandPoint>rightHandPoint){//right가 더 가까울때
                    result += "R  "
                    rightHandTouched = numbers.get(i)
                }else{//같은 거리일때
                    result += handInSolution
                    if(handInSolution.equals("L  ")){
                        leftHandTouched = numbers.get(i)
                    }else{
                        rightHandTouched = numbers.get(i)
                    }
                }
            }
        }
        var answer = result
        return answer
    }

    fun getPoint(touchedNum:Int,currentyNum:Int):Int {
        for (i in 0 until pad_numbers.get(touchedNum).size) {
            if (pad_numbers.get(touchedNum).get(i).contains(currentyNum)) {
                return i
            }
        }
        return 0
    }



}//class end


