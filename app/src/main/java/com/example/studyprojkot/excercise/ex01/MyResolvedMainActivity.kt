package com.example.studyprojkot.excercise.ex01

import android.app.Activity
import android.os.Bundle
import android.util.Log
import com.example.studyprojkot.R

class MyResolvedMainActivity : Activity() {

    val ex1 = "...!@BaT#*..y.abcdefghijklm"
    //=> "bat.y.abcdefghi"

    val ex2 = "z-+.^."
    //=> "z--"

    val ex3 = "=.="
    //=> "aaa"

    val ex4 = "123_.def"
    //=> "123_.def"

    val ex5 = "abcdefghijklmn.p"
    //=> "abcdefghijklmn"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.test_main)

        var testValue = ex5
        Log.e("final_yck","MyTestMainActivity/testValue : ${testValue}")

        //1.소문자로 치환
        val step1Data = step1(testValue)
        Log.e("testValue","step1Data : ${step1Data}")

        //2.알파벳 소문자, 숫자, 빼기(-), 밑줄(_), 마침표(.) 를 제외한 나머지 제거
        val step2Data = step2(step1Data)
        Log.e("testValue","step2Data : ${step2Data}")

        //3. 마침표(.)가 2번 이상 연속된 부분을 하나의 마침표(.)로 치환
        val step3Data = step3(step2Data)
        Log.e("testValue","step3Data : ${step3Data}")

        //4. 마침표(.)가 처음이나 끝에 위치한다면 제거합니다.
        val step4Data = step4(step3Data)
        Log.e("testValue","step4Data : ${step4Data}")

        //5. 빈 문자열이라면, new_id에 "a"를 대입합니다.
        val step5Data = step5(step4Data)
        Log.e("testValue","step5Data : ${step5Data}")

        //6. 길이가 16자 이상이면, 첫 15개의 문자를 제외한 나머지 문자들을 모두 제거합니다.
        val step6Data = step6(step5Data)
        Log.e("testValue","step6Data : ${step6Data}")

        //7.길이가 2자 이하라면, 마지막 문자를 길이가 3이 될 때까지 반복해서 끝에 붙임
        val step7 = step7(step6Data)
        Log.e("final_yck","MyTestMainActivity/step7 : ${step7}")
    }

    //1. 모든 대문자를 대응되는 소문자로 치환
    fun step1(value:String):String{
        if(value==null||value.isEmpty()) return ""
        return value.toLowerCase()
    }

    //2. 알파벳 소문자, 숫자, 빼기(-), 밑줄(_), 마침표(.)를 제외한 모든 문자를 제거
    fun step2(value:String):String{
        if(value==null||value.isEmpty()) return ""
        var result = ""
        val reg = Regex("^([a-z]|[0-9]|[._-]?)$")
        for(i in 0 until value.length){
            if(reg.matches(value.substring(i,i+1))){
                result += value.substring(i,i+1)
            }
        }
        return result
    }

    //3. 마침표(.)가 2번 이상 연속된 부분을 하나의 마침표(.)로 치환
    fun step3(value:String):String{
        if(value==null||value.isEmpty()) return ""
        var result = ""
        var isCommaStarted = false
        for (i in 0 until value.length){
            if(value.substring(i,i+1).equals(".")){
                if(isCommaStarted){
                    continue
                }
                result += value.substring(i,i+1)
                isCommaStarted = true
                continue
            }
            result += value.substring(i,i+1)
            isCommaStarted = false
        }
        Log.d("step3","${result}")
        return result
    }

    //4. 마침표(.)가 처음이나 끝에 위치한다면 제거합니다.
    fun step4(value:String):String{
        if(value==null||value.isEmpty()) return ""
        var result = value
        var finalResult = ""
        if(value.substring(0,1).equals(".")){
            result = value.drop(1)
        }
        finalResult = result
        if(result.isEmpty()) return ""
        if(result.substring(result.length-1,result.length).equals(".")){
            finalResult = result.dropLast(1)
        }
        return finalResult.trim()
    }

    //5. 빈 문자열이라면, new_id에 "a"를 대입합니다.
    fun step5(value:String):String{
        if(value==null||value.isEmpty()) return "a"
        return value
    }

    //6. 길이가 16자 이상이면, 첫 15개의 문자를 제외한 나머지 문자들을 모두 제거합니다.
    //     만약 제거 후 마침표(.)가 new_id의 끝에 위치한다면 끝에 위치한 마침표(.) 문자를 제거합니다.
    fun step6(value:String):String{
        if(value==null||value.isEmpty()) return ""
        if(value.length<16) return value
        var result = ""
        var finalResult = ""
        result = value.substring(0,15)
        if(result.substring(result.length-1,result.length).equals(".")){
            finalResult = result.dropLast(1)
            return finalResult
        }
        return result
    }

    //7.길이가 2자 이하라면, 마지막 문자를 길이가 3이 될 때까지 반복해서 끝에 붙임
    fun step7(value:String):String{
        if(value==null||value.isEmpty()) return ""
        if(value.length>2) return value
        var result = ""
        //ab
        if(value.length==2){
            val addString = value.substring(value.length-1,value.length)
            result = value+addString
            return result
        }
        result = value+value+value
        return result
    }


}//class end