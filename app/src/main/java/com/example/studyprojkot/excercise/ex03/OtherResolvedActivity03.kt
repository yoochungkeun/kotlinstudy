package com.example.studyprojkot.excercise.ex03

import android.app.Activity
import android.os.Bundle
import android.util.Log
import kotlin.math.abs

class OtherResolvedActivity03:Activity() {

    var ex01_stages = intArrayOf(2,1,2,6,2,4,3,3)
    var ex02_stages = intArrayOf(4,4,4,4,4)
/*
    N	stages	                    result
    5	[2, 1, 2, 6, 2, 4, 3, 3]	[3,4,2,1,5]
    4	[4,4,4,4,4]	                [4,1,2,3]
*/

    data class Stage(var level: Int, var pass: Int, var fail: Int) {
        val failRate: Float
            get() = if (fail+pass == 0)  0.0f else (fail.toFloat()) / (pass + fail)
    }

    fun solution(N: Int, stages: IntArray): IntArray {
        var stageInfo = Array(N,  { Stage(it+1, 0, 0)})

        for (level in stages) {
            for (i in 0.until(level-1)) {
                stageInfo[i].pass++
            }
            if (level != N+1) stageInfo[level-1].fail++
        }
        stageInfo.sortByDescending { it.failRate }
        return stageInfo.map { it.level }.toIntArray()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        solution(4,ex01_stages)
    }


}//class end