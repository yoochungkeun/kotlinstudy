package com.example.studyprojkot.excercise.ex02

import android.app.Activity
import android.os.Bundle
import android.util.Log
import kotlin.math.abs

class OtherResolvedActivity02:Activity() {


    val ex1_numbers_hand = listOf(intArrayOf(1, 3, 4, 5, 8, 2, 1, 4, 5, 9, 5),"right")
    //=> "L  R  L  L  L  R  L  L  R  R  L"
    val ex2_numbers_hand = listOf(intArrayOf(7, 0, 8, 2, 8, 3, 1, 5, 7, 6, 2),"left")
    //=> "L  R  L  L  R  R  L  L  L  R  R"
    val ex3_numbers_hand = listOf(intArrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 0),"right")
    //=> "L  L  R  L  L  R  L  L  R  L"

    val info = mapOf(0 to Pair(1,3),1 to Pair(0,0),2 to Pair(1,0),3 to Pair(2,0),
        4 to Pair(0,1),5 to Pair(1,1),6 to Pair(2,1),7 to Pair(0,2),8 to Pair(1,2),9 to Pair(2,2))


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var testValue = ex1_numbers_hand

        solution(testValue[0] as IntArray,testValue.get(1).toString())
    }


    fun solution(numbers: IntArray, hand: String): String {
        val lefthanded = hand == "left"
        var nowLeft = Pair(0,3)
        var nowRight = Pair(2,3)

        return numbers.map {
            if(info[it]!!.first==0){
                nowLeft = info[it]!!
                'L'
            }else if(info[it]!!.first==2){
                nowRight = info[it]!!
                'R'
            }else{
                val distLeft = abs(info[it]!!.first-nowLeft.first) + abs(info[it]!!.second-nowLeft.second)
                val distRight = abs(info[it]!!.first-nowRight.first) + abs(info[it]!!.second-nowRight.second)
                if(distLeft>distRight){
                    nowRight = info[it]!!
                    'R'
                }else if(distLeft<distRight){
                    nowLeft = info[it]!!
                    'L'
                }else{
                    if(lefthanded){
                        nowLeft = info[it]!!
                        'L'
                    }else{
                        nowRight = info[it]!!
                        'R'
                    }
                }
            }
        }.joinToString("")
    }

}