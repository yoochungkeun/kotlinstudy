package com.example.studyprojkot

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

open class BaseActivity:AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //ActionBar Title 해당 클래스로 변경하는 코드
        var actionBar = supportActionBar
        actionBar?.title = javaClass.simpleName




    }


}//class end