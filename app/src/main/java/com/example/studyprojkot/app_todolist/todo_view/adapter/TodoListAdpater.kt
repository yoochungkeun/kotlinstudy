package com.example.studyprojkot.ex_todo.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.studyprojkot.R
import com.example.studyprojkot.app_todolist.todo_db.TodoContacts
import com.example.studyprojkot.app_todolist.todo_listener.TodoOnItemClickListener
import com.example.studyprojkot.ex_todo.view.holder.TodoViewHolder

class TodoListAdpater (val todoContactsList: List<TodoContacts>):RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    //RecyleView 의 list item 클릭시 연결시킬 프로퍼티(=타입이 지정된 변수)
    private lateinit var mItemClickListener: TodoOnItemClickListener

    //implement method : RecyleView 의 list 셋팅 될때 , 처음 호출되는 함수
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_todo,parent,false)
        val viewHolder = TodoViewHolder(view)
        return viewHolder
    }

    //implement method : RecyleView 의 list item size
    override fun getItemCount(): Int {
        return todoContactsList.size
    }

    //implement method : RecyleView 의 list item position 을 별도로 생성한 viewHodler 와 연결
    override fun onBindViewHolder(bindholder: RecyclerView.ViewHolder, position: Int) {
        val todoContacts = todoContactsList[position]
        val todoViewHolder = bindholder as TodoViewHolder
        todoViewHolder.bind(todoContacts)
        //RecyleView 의 list item  클릭 이벤트 연결
        todoViewHolder.itemView.setOnClickListener {
            mItemClickListener.onClickListener(it,position)
        }
    }

    //RecyleView 의 list item  클릭 이벤트 연결(interface로 생성한 TodoOnItemClickListener 를 이용한 연결)
    fun setItemClick(itemClickListener: TodoOnItemClickListener){
        mItemClickListener = itemClickListener
    }

}//class end