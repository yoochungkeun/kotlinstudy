package com.example.studyprojkot.app_todolist.todo_db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

//@Database => (데이터베이스) 저장하는 데이터의 집합 단위를 말한다.
@Database(entities = [TodoContacts::class], version = 1, exportSchema = false)
abstract class TodoDatabase : RoomDatabase(){

    abstract fun todo_contact_dao():TodoContactsDao

    companion object{
        private var instance: TodoDatabase? = null
        @Synchronized
        fun getInstance(context: Context): TodoDatabase?{
            if(instance==null){
                instance = Room.databaseBuilder(
                    context.applicationContext,
                    TodoDatabase::class.java,
                    "database-todo_contacts"
                ).allowMainThreadQueries()
                    .build()
            }
            return instance
        }
    }
}