package com.example.studyprojkot.app_todolist.todo_db

import androidx.room.Entity
import androidx.room.PrimaryKey

//@Entity => (항목) 데이터베이스 내의 테이블을 의미한다.
@Entity(tableName = "tb_todo_contacts")
data class TodoContacts (
    @PrimaryKey(autoGenerate = true) val id:Long,
    var title:String,
    var description:String,
    var createdDate:String
)
