package com.example.studyprojkot.ex_todo.view

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.studyprojkot.R
import com.example.studyprojkot.app_todolist.todo_db.TodoContacts
import com.example.studyprojkot.app_todolist.todo_db.TodoDatabase
import com.example.studyprojkot.app_todolist.todo_listener.TodoOnItemClickListener
import com.example.studyprojkot.ex_todo.view.adapter.TodoListAdpater
import kotlinx.android.synthetic.main.activity_todo_main.*
import kotlinx.android.synthetic.main.dialog_add_todo.view.*
import java.text.SimpleDateFormat
import java.util.*

class TodoMainActivity : AppCompatActivity() {

    private lateinit var mTodoListAdapter: TodoListAdpater
    private lateinit var mTodoContacts: TodoContacts
    private var mDB: TodoDatabase? =null
    private var mTodoContactList = mutableListOf<TodoContacts>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_todo_main)
        var actionBar = supportActionBar
        actionBar!!.title = javaClass.simpleName

        initDB()
        initRecyclerView()

        btn_add_todo.setOnClickListener {
            openAddItemDialog()
        }
    }

    //RecylerView 초기화
    private fun initRecyclerView() {
        mTodoListAdapter = TodoListAdpater(mTodoContactList)
        rl_todo_list.run {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@TodoMainActivity)
            adapter = mTodoListAdapter
        }
        mTodoListAdapter.setItemClick(onitemClick())
    }

    //db 초기화
    fun initDB() {
        mDB = TodoDatabase.getInstance(this)
        //이전에 저장한 내용 모두 불러와서 추가하기
        val savedTodoContacts = mDB!!.todo_contact_dao().getAll()
        if (savedTodoContacts.isNotEmpty()) {
            mTodoContactList.addAll(savedTodoContacts)
        }
    }

    //open the Add Dialog
    private fun openAddItemDialog() {
        val dialogView = layoutInflater.inflate(R.layout.dialog_add_todo, null)
        val dialog = AlertDialog.Builder(this)
            .setTitle("추가하기")
            .setView((dialogView))
            .setPositiveButton("확인", { dialogInterface, i ->
                setData(dialogView)
                addToDB()
            })
            .setNegativeButton("취소", null)
            .create()
        dialog.show()
    }

    //시간 -> String 변환
    fun Long.toDateString(format: String): String {
        val simpleDateFormat = SimpleDateFormat(format)
        return simpleDateFormat.format(Date(this))
    }

    //생성한 interface onItemClickListenerClick 을 함수로 만들어 RecylerView list item 과 연결시켜 이벤트 처리하기 위함
    fun onitemClick(): TodoOnItemClickListener = object :TodoOnItemClickListener{
        override fun onClickListener(v: View, position: Int) {
          val todocontacts = mTodoContactList[position]
            mDB?.todo_contact_dao()?.delete(todocontacts)
            mTodoContactList.removeAt(position)
            mTodoListAdapter.notifyDataSetChanged()
        }
    }

    //Data 셋팅
    fun setData(dialogView:View){
        val title = dialogView.et_todo_title.text.toString()
        val description = dialogView.et_todo_description.text.toString()
        val createDate = Date().time
        val strCreateDate = createDate.toDateString("yyyy.MM.dd HH:mm")
        mTodoContacts = TodoContacts(
            id = 0,title = title,description = description,createdDate = strCreateDate
        )
    }

    //DB에 추가하기
    fun addToDB(){
        val todoContacts: TodoContacts = mTodoContacts ?: return
        mDB?.todo_contact_dao()?.insertAll(todoContacts)
        mTodoContactList.add(todoContacts)
        mTodoListAdapter.notifyDataSetChanged()
    }

}//class end