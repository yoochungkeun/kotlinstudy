package com.example.studyprojkot.app_todolist.todo_listener

import android.view.View

interface TodoOnItemClickListener {
    fun onClickListener(v: View, position:Int)
}