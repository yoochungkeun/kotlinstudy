package com.example.studyprojkot.ex_todo.view.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.studyprojkot.app_todolist.todo_db.TodoContacts
import kotlinx.android.synthetic.main.item_todo.view.*

/*
ViewHodler 는 리스트를 스크롤 하는 동안 뷰를 생성하고
다시 뷰의 구성요소를 찾는 행위를 반복하면서 생기는 성능저하를
방지하기 위해 미리 저장해놓고 빠르게 접근하기 위해서 사용하는 객체다
*/
class TodoViewHolder(view:View):RecyclerView.ViewHolder(view) {

    val title = view.tv_todo_title
    val description = view.tv_todo_description
    val createdDate = view.tv_todo_created_date

    fun bind(todoContactsList: TodoContacts){
        title.text = todoContactsList.title
        description.text = todoContactsList.description
        createdDate.text = todoContactsList.createdDate
    }

}//class end