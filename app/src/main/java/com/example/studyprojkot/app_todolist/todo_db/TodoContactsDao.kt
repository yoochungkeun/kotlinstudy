package com.example.studyprojkot.app_todolist.todo_db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

//@Dao => (다오) 데이터베이스에 접근하는 함수(insert,update,delete,...)를 제공한다.
@Dao
interface TodoContactsDao {
    @Query("SELECT * FROM tb_todo_contacts")
    fun getAll():List<TodoContacts>

    @Insert
    fun insertAll(vararg todoContacts: TodoContacts)

    @Delete
    fun delete(tb_todo_contacts: TodoContacts)
}