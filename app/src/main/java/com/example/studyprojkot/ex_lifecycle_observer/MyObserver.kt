package com.example.studyprojkot.ex_lifecycle_observer

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import com.example.studyprojkot.BaseConstant

class MyObserver(private val lifeCycle:Lifecycle):LifecycleObserver {

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreated(source: LifecycleOwner){
        Log.d(BaseConstant.LIFECYCLE_OBSERVER_TAG,"MyObserver/onCreated()")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart(){
        Log.d(BaseConstant.LIFECYCLE_OBSERVER_TAG,"MyObserver/onStart()")
        if(lifeCycle.currentState.isAtLeast(Lifecycle.State.INITIALIZED)){
            Log.d(BaseConstant.LIFECYCLE_OBSERVER_TAG,"MyObserver/currentState:INITIALIZED")
        }
        if(lifeCycle.currentState.isAtLeast(Lifecycle.State.CREATED)){
            Log.d(BaseConstant.LIFECYCLE_OBSERVER_TAG,"MyObserver/currentState:CREATED")
        }
        if(lifeCycle.currentState.isAtLeast(Lifecycle.State.STARTED)){
            Log.d(BaseConstant.LIFECYCLE_OBSERVER_TAG,"MyObserver/currentState:STARTED")
        }
        if(lifeCycle.currentState.isAtLeast(Lifecycle.State.RESUMED)){
            Log.d(BaseConstant.LIFECYCLE_OBSERVER_TAG,"MyObserver/currentState:RESUMED")
        }
        if(lifeCycle.currentState.isAtLeast(Lifecycle.State.DESTROYED)){
            Log.d(BaseConstant.LIFECYCLE_OBSERVER_TAG,"MyObserver/currentState:DESTㄴROYED")
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume(){
        Log.d(BaseConstant.LIFECYCLE_OBSERVER_TAG,"MyObserver/onResume()")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause(){
        Log.d(BaseConstant.LIFECYCLE_OBSERVER_TAG,"MyObserver/onPause()")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop(){
        Log.d(BaseConstant.LIFECYCLE_OBSERVER_TAG,"MyObserver/onStop()")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy(){
        Log.d(BaseConstant.LIFECYCLE_OBSERVER_TAG,"MyObserver/onDestroy()")
    }



}//class end