package com.example.studyprojkot.ex_lifecycle_observer

import android.os.Bundle
import com.example.studyprojkot.R
import com.example.studyprojkot.BaseActivity

class LifeCycleObserverActivityMain : BaseActivity() {

    val observer by lazy {
        MyObserver(lifecycle)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lifecycle_observer_main)

        lifecycle.addObserver(observer)

    }


}//class end